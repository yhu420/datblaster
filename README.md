#DatBlaster

DatBlaster will be a sort of shmup game engine. I'm a beginner and I need some help with this project, if you're learning too, don't hesitate to give me a hand if you want to.
Also, the name is very likely to change

**Features:**

* 	Animated Sprites
* 	Wave Control
* 	Pattern control

**Todo list:**

* 	Advanced resolution changes
* 	Backgrounds, parallax, for menu and game
* 	Pattern editor
* 	Sprite editor
* 	CFG editor w/ help
* 	Implement 'walkable' areas