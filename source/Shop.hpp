#pragma once 
#include "RSprite.hpp"
#include "GlobalVars.h"
#include "MenuButton.hpp"
#include "MenuNav.hpp"
#include <vector>

class Shop : public sf::Drawable
{
public:
	Shop();
	~Shop();

	void setButtonCount(const int short unsigned& count);
	void setRelativeHoveredShape(const short int& shapenum);
	void setAbsoluteHoveredShape(const short unsigned int& shapenum);

	MenuNav* getNav() const{ return m_nav;};

	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		target.draw(*m_background);
		target.draw(m_shopTitle);
		target.draw(*m_nav);
		for(int short unsigned i=0U; i<m_buttonList.size()-1U; ++i)
		{
			target.draw(*m_buttonList[i]);
		}
	}

private:
	short unsigned int m_hoveredShape;

	RSprite* m_background;

	sf::Text m_shopTitle;

	///
	std::vector<MenuButton*> m_buttonList;
	sf::Texture* m_butTexNormal;
	sf::Texture* m_butTexHovered;
	sf::Texture* m_butTexActivated;
	///

	///
	MenuNav* m_nav;
	sf::Texture* m_navBackTex;
	sf::Texture* m_navExtraTex;
	sf::Texture* m_navHullTex;
	sf::Texture* m_navGunzTex;
	///
};