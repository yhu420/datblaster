#pragma once
#include <SFML/Graphics.hpp>
#include "RSprite.hpp"
#include "GValueList.hpp"
#include "AskKeyWindow.hpp"

class SettingsMenu : public sf::Drawable
{
public:
	SettingsMenu();
	~SettingsMenu();

	void setMainm_optionstxtList();
	void setControlsOptionList();
	void saveTheKeyBindings();
	void askKey();
	void run();

	void selectRelativeOption(const short int& i);
	void selectAbsoluteOption(const short unsigned int& i);

	short int getCurrentIndex() const {return m_valueList.getOptionIndex();};
	bool isOnMainList() const {return m_valueList.getMainList();};
	void changeRightOptionText(const short int index, const std::string newOptionDescription);
	bool noDoubleKeys() const;

	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		target.draw(m_background);
		target.draw(m_title);
		target.draw(m_valueList);
	}
private:
	std::string intKeyToStringKey(const int& theInt);

	RSprite m_background;
	GValueList m_valueList;
	sf::Text m_title;
	AskKeyWindow* m_askwindow;

	sf::Keyboard::Key m_goUp;
	sf::Keyboard::Key m_goDown;
	sf::Keyboard::Key m_goRight;
	sf::Keyboard::Key m_goLeft;
	sf::Keyboard::Key m_fire;
	sf::Keyboard::Key m_special;
};