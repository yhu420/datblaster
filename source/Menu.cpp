#include "Menu.hpp"

Menu::Menu() : 
	isAtMain(true), 
	isAtCredits(false), 
	isAtSettings(false), 
	isAtShop(false), 
	isUsingTheMenu(true), 
	m_doorsOpenedOnce(false)
{
	createMain();
	timeLastPressed = new sf::Clock;

	m_sBuffer = new sf::SoundBuffer();
	m_sBuffer->loadFromFile("resources/sound/effects/menuchange.wav");
	m_sound = new sf::Sound(*m_sBuffer);

	m_doorsBuffer = new sf::SoundBuffer();
	m_doorsBuffer->loadFromFile("resources/sound/effects/closedoors.wav");
	m_doorSound = new sf::Sound(*m_doorsBuffer);
}

Menu::~Menu()
{
	Var.music.stop();
	std::this_thread::sleep_for(std::chrono::milliseconds(3000));

	//Doors
	delete rightDoor;
	rightDoor = nullptr;
	delete leftDoor;
	leftDoor = nullptr;

	//System
	delete timeLastPressed;
	timeLastPressed = nullptr;

	//Sounds
	delete m_sound;
	m_sound = nullptr;
	delete m_doorSound;
	m_doorSound = nullptr;

	//Buffers
	delete m_doorsBuffer;
	m_doorsBuffer = nullptr;
	delete m_sBuffer;
	m_sBuffer = nullptr;

	//The rest
	delMain();
	if (Var.wantsMusic)
		loadMusic();
}

void Menu::delMain()
{
	isAtMain = false;

	delete m_mainMenu;
	m_mainMenu = nullptr;
}

void Menu::delSettings()
{
	delete m_settingsMenu;
	m_settingsMenu = nullptr;
}

void Menu::delCredits()
{
	delete m_creditsMenu;
	m_creditsMenu = nullptr;
}

void Menu::delShop()
{
	delete m_shop;
	m_shop = nullptr;
}

void Menu::createCredits()
{
	if(!m_creditsMenu)
		m_creditsMenu = new CreditsMenu;
}

void Menu::createMain()
{
	if (!m_mainMenu)
		m_mainMenu = new MainMenu;
}

void Menu::createSettings()
{
	if(!m_settingsMenu)
		m_settingsMenu = new SettingsMenu;
}

void Menu::createShop()
{
	if(!m_shop)
		m_shop = new Shop;
}

void Menu::runMain()
{
	if(!!leftDoor && !m_doorsOpenedOnce)
	{
		openDoors();
	}
	
	//Manage the colors highlight, does this through the end of runmain()
	
	if(sf::Keyboard::isKeyPressed(Var.goDown) && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f)
	{
		m_mainMenu->selectEntryUnder();
		m_sound->play();
		timeLastPressed->restart();
	}
	else if(sf::Keyboard::isKeyPressed(Var.goUp) && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f)
	{
		m_mainMenu->selectEntryAbove();
		m_sound->play();
		timeLastPressed->restart();
	}
	else if(m_mainMenu->getEntry() == 0 && sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{
		closeDoors();
		//Doors are closed, let's destroy everything BUT THE DOORS
		
		delMain();
		isUsingTheMenu = false;
		//Now Game.cpp will handle the following
	}
	else if(m_mainMenu->getEntry() == 1 && sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{
		closeDoors();
		//Doors are closed, let's destroy everything BUT THE DOORS
		
		delMain();
		createShop();
		isAtMain = false;
		isAtShop = true;
		//Loading done
	}
	else if(m_mainMenu->getEntry() == 2 && sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{
		closeDoors();

		delMain();
		createSettings();
		isAtMain = false;
		isAtSettings = true;
	}
	else if(m_mainMenu->getEntry() == 3 && sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{
		closeDoors();
		//Doors are closed, let's destroy everything BUT THE DOORS
		
		delMain();
		createCredits();
		isAtMain = false;
		isAtCredits = true;
		//Loading done
	}
	else if (m_mainMenu->getEntry() == 4 && sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{
		//Exit the game
		exit(EXIT_SUCCESS);
	}
	else
	{
		m_mainMenu->run();
	}
}

void Menu::runShop()
{
	if(!!leftDoor && !m_doorsOpenedOnce)
	{
	openDoors();
	//first open the doors
	}
	//Then go: are displayed : m_shop
	if(m_shop->getNav()->getHaveFocus() && sf::Keyboard::isKeyPressed(Var.goUp) && m_shop->getNav()->getFocusedShape() < 3 && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f)
	{
		//Nav select the one up
		m_shop->getNav()->giveFocusToShape(m_shop->getNav()->getFocusedShape() + 1);
		timeLastPressed->restart();
	}

	else if(m_shop->getNav()->getHaveFocus() && sf::Keyboard::isKeyPressed(Var.goDown) && m_shop->getNav()->getFocusedShape() > 0 && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f)
	{
		//Nav select the one down
		m_shop->getNav()->giveFocusToShape(m_shop->getNav()->getFocusedShape() - 1);
		timeLastPressed->restart();
	}
	else if(m_shop->getNav()->getHaveFocus() && sf::Keyboard::isKeyPressed(Var.goRight) && timeLastPressed->getElapsedTime().asSeconds() >= 0.15f)
	{
		//If pressed right, and is currently hovering the nav
		m_shop->getNav()->deactivate();
		m_shop->setAbsoluteHoveredShape(0U);
		timeLastPressed->restart();
	}
	else if(!m_shop->getNav()->getHaveFocus() && sf::Keyboard::isKeyPressed(Var.goRight) && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f)
	{
		m_shop->setRelativeHoveredShape(1);
		timeLastPressed->restart();
	}
	else if(!m_shop->getNav()->getHaveFocus() && sf::Keyboard::isKeyPressed(Var.goLeft) && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f)
	{
		m_shop->setRelativeHoveredShape(-1);
		timeLastPressed->restart();
	}
	else if(!m_shop->getNav()->getHaveFocus() && sf::Keyboard::isKeyPressed(Var.goDown) && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f)
	{
		m_shop->setRelativeHoveredShape(4);
		timeLastPressed->restart();
	}
	else if(!m_shop->getNav()->getHaveFocus() && sf::Keyboard::isKeyPressed(Var.goUp) && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f)
	{
		m_shop->setRelativeHoveredShape(-4);
		timeLastPressed->restart();
	}
	else if(!m_shop->getNav()->getHaveFocus() && sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f)
	{
		m_shop->getNav()->activate();
		timeLastPressed->restart();
	}
	//EXITING
	else if(m_shop->getNav()->getHaveFocus() && m_shop->getNav()->getFocusedShape() == 3U && sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f)
	{
		closeDoors(); //Doors closed
		timeLastPressed->restart();
		delShop();
		createMain();
		isAtShop = false;
		isAtMain = true;
		m_doorsOpenedOnce = false;
	}
}

void Menu::runCredits()
{
	if(!!leftDoor && !m_doorsOpenedOnce)
	{
	openDoors();
	//first open the doors
	}

	m_creditsMenu->displayCredits();

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		closeDoors();
		//Doors closed, let's clean things up
		delCredits();
		createMain();
		isAtCredits = false;
		isAtMain = true;
		m_doorsOpenedOnce = false;

		timeLastPressed->restart();
	}
}

void Menu::runSettings()
{
	if(!!leftDoor && !m_doorsOpenedOnce)
	{
	openDoors();
	//first open the doors
	}

	if(sf::Keyboard::isKeyPressed(Var.goDown) && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f)
	{
		m_settingsMenu->selectRelativeOption(1);
		timeLastPressed->restart();
	}
	else if(sf::Keyboard::isKeyPressed(Var.goUp) && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f)
	{
		m_settingsMenu->selectRelativeOption(-1);
		timeLastPressed->restart();
	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && m_settingsMenu->getCurrentIndex() == 0 && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f && m_settingsMenu->isOnMainList())
	{
		//Firts choice of first menu
		m_settingsMenu->setControlsOptionList();
		timeLastPressed->restart();
	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && m_settingsMenu->getCurrentIndex() == 1 && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f && m_settingsMenu->isOnMainList())
	{
		//second choice of first menu
		if(Var.music.getStatus() == sf::SoundSource::Playing)
		{
			Var.music.stop();
			m_settingsMenu->changeRightOptionText(1, std::string("Off"));
			Var.wantsMusic = false;
		}
		else
		{
			loadMusic();
			m_settingsMenu->changeRightOptionText(1, std::string("On"));
			Var.wantsMusic = true;
		}
		timeLastPressed->restart();
	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && m_settingsMenu->getCurrentIndex() == 2 && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f && m_settingsMenu->isOnMainList())
	{
		//Third choice of first menu
		closeDoors();
		//Doors closed, let's clean things up
		delSettings();
		createMain();
		isAtSettings = false;
		isAtMain = true;
		m_doorsOpenedOnce = false;

		timeLastPressed->restart();
	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && m_settingsMenu->getCurrentIndex() >= 0 && m_settingsMenu->getCurrentIndex() <= 5 && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f && !m_settingsMenu->isOnMainList())
	{
		//First 6 choices of second menu (keys to bind)
		m_settingsMenu->askKey();
		timeLastPressed->restart();
	}
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && m_settingsMenu->getCurrentIndex() == 6 && timeLastPressed->getElapsedTime().asSeconds() >= 0.2f && !m_settingsMenu->isOnMainList())
	{
		//if wants to go back to main options list
		if(m_settingsMenu->noDoubleKeys())
		{
			m_settingsMenu->saveTheKeyBindings();
			m_settingsMenu->setMainm_optionstxtList();
			timeLastPressed->restart();
		}
		else
		{
			//emit DENIED sound
			Var.errorMgr.playCantSound();
			timeLastPressed->restart();
		}
	}
}

void Menu::run()
{
	if(isAtMain)
	{
		runMain();
	}
	else if(isAtCredits)
	{
		runCredits(); //This is puke. Gonna have to rewrite it like m_creditsmenu.run();
	}
	else if(isAtSettings)
	{
		runSettings();
	}
	else if(isAtShop)
	{
		runShop();
	}
	else
	{
		std::cerr << "menu.cpp, leaving the menu" << std::endl;
	}
}

bool Menu::getisUsing() const
{
	return isUsingTheMenu;
}

void Menu::closeDoors()
{
	if(!leftDoor)
	{
		leftDoor = new RSprite(std::string("resources/images/menu/leftload.jpg"), 500, 600);
		leftDoor->setPosition(-500.f, 0.f);
		rightDoor = new RSprite(std::string("resources/images/menu/rightload.jpg"), 500, 600);
		rightDoor->setPosition(1000.f,0.f);
	}

	while(leftDoor->getPosition().x < 0.f)
	{
		leftDoor->move(1000.f * Var.deltaTime , 0.f);
		rightDoor->move(-1000.f * Var.deltaTime, 0.f);
		Var.toWindow->draw(*leftDoor);
		Var.toWindow->draw(*rightDoor);
		Var.toWindow->display();
	}
	leftDoor->setPosition(0,0);
	rightDoor->setPosition(500,0);
	m_doorSound->play();
	Var.toWindow->clear();
	Var.toWindow->draw(*leftDoor);
	Var.toWindow->draw(*rightDoor);
	Var.toWindow->display();
	m_doorsOpenedOnce = false;
}

void Menu::openDoors()
{
	if(!!leftDoor)
	{
		leftDoor->move(-8.f, 0.f); //TO SET BACK
		rightDoor->move(8.f, 0.f);
	}

	if(!!leftDoor && leftDoor->getPosition().x < -510.f)
	{
		std::cerr << "deleting doors\n";
		delete rightDoor;
		rightDoor=nullptr;

		delete leftDoor;
		leftDoor=nullptr;
		m_doorsOpenedOnce = true;
	}
}