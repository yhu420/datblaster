#include "Pattern.hpp"

Pattern::Pattern(std::vector<sf::Vector2f> const& valueTable, std::vector<float> const& speedy, const float& imprecision) :
	m_lastNodeIndex(0),
	m_nodes(valueTable),
	m_speedModif(speedy)
{
	m_speedModif.resize(m_nodes.size() - 1, 1.f);
	if (imprecision != 0.f)
		shufflePattern(imprecision);
}

Pattern Pattern::shufflePattern(float const& radiusToShuffle) const
{
	std::vector<sf::Vector2f> tempPos;
	for (std::size_t i = 0; i < m_nodes.size(); i++)
	{
		tempPos.push_back(sf::Vector2f(
			randNumber(m_nodes[i].x - radiusToShuffle, m_nodes[i].x + radiusToShuffle),
			randNumber(m_nodes[i].y - radiusToShuffle, m_nodes[i].y + radiusToShuffle)
									   ));
	}
	return Pattern(tempPos, m_speedModif);
}

void Pattern::shuffleSelfPattern(float const& radiusToShuffle)
{
	for (std::size_t i = 0; i < m_nodes.size(); i++)
	{
		m_nodes[i] = (sf::Vector2f(
			randNumber(m_nodes[i].x - radiusToShuffle, m_nodes[i].x + radiusToShuffle),
			randNumber(m_nodes[i].y - radiusToShuffle, m_nodes[i].y + radiusToShuffle)
		));
	}
}

sf::Vector2f Pattern::getMovingVector(sf::Vector2f const& currentPos)
{
	//PRECHECK
	//Has it reached the end?
	if (reachedEnd())
		return sf::Vector2f(0.f, 0.f);

	//STEP 1
	//Calculate the moving vector
	
	const sf::Vector2f movinvec(
		(m_nodes[m_lastNodeIndex + 1].x - currentPos.x) / (sqrt(pow(m_nodes[m_lastNodeIndex + 1].x - currentPos.x, 2) + pow(m_nodes[m_lastNodeIndex + 1].y - currentPos.y, 2))),
		(m_nodes[m_lastNodeIndex + 1].y - currentPos.y) / (sqrt(pow(m_nodes[m_lastNodeIndex + 1].x - currentPos.x, 2) + pow(m_nodes[m_lastNodeIndex + 1].y - currentPos.y, 2)))
	);

	//STEP 2
	//Check if actor reached the next node

	if (sqrt(pow(m_nodes[m_lastNodeIndex + 1].x - currentPos.x, 2) + pow(m_nodes[m_lastNodeIndex + 1].y - currentPos.y, 2))<10.f)
	{
		m_lastNodeIndex++;
	}

	// STEP #3
	// Return the value, with the modifiers
	return movinvec*m_speedModif[m_lastNodeIndex];
}

void Pattern::setNodePos(std::size_t const& index, sf::Vector2f const& newpos)
{
	m_nodes[index] = newpos;
}

bool Pattern::reachedEnd() const
{
	return m_lastNodeIndex + 1 == m_nodes.size();
	/*if (m_lastNodeIndex+1 == m_nodes.size())
	{
		return true;
	}
	return false*/
}

void Pattern::moveAllNodesSoFirstOneIsLike(sf::Vector2f const& first) //check if is correct
{
	const sf::Vector2f moveoffset(first - m_nodes[0]);
	for (size_t i = 0; i < m_nodes.size(); i++)
	{
		m_nodes[i] += moveoffset;
	}
}