#pragma once

#include <SFML/Graphics.hpp>
#include <vector>
#include "RSprite.hpp"
#include "GlobalVars.h"


//Graphical list of values. Much like an array with 2 columns, filled with strings

class GValueList : public sf::Drawable
{
public:
	GValueList(const short unsigned int& width, const sf::Vector2f& position);
	~GValueList();

	short int getOptionIndex() const {return m_hoveredBox;};

	sf::Text getRightColumnValue(size_t const& index);
	void clear() {m_keys.clear(); m_numOfChoices=0U; sort();};
	void addValue(const sf::String& key, const sf::String& value, const sf::Color& textcolor = sf::Color(125, 143, 255));
	void setRelativeHoveredBox(const short int& i);
	void setAbsoluteHoveredBox(const short unsigned int& i);
	void modifyOptionState(const short int index, const std::string newOptionDescription);

	short unsigned int getNumOfChoices() const{return m_numOfChoices;};
	void setMainList(const bool& yn) {m_isMainList=yn;};
	bool getMainList() const{return m_isMainList;};

	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		unsigned short i;
		for(i=0U; i<m_hrs.size();++i)
		{
			target.draw(m_hrs[i]);
		}
		target.draw(m_hoverBox);
		for(i = 0U; i<m_keys.size(); ++i)
		{
			target.draw(m_keys[i].first);
			target.draw(m_keys[i].second);
		}
	}
private:
	void sort();
	bool m_isMainList;

	short unsigned int m_width;
	short unsigned int m_numOfChoices;
	sf::Vector2f m_position;

	std::vector<sf::RectangleShape> m_hrs;
	std::vector<std::pair<sf::Text, sf::Text> > m_keys;

	sf::ConvexShape m_hoverBox;
	short int m_hoveredBox;
};