#include "Game.hpp"

Game::Game()
{
	m_menu = new Menu;
}

void Game::runMenu() //At this point, the menu is already created
{
	m_menu->run();
}

void Game::run()
{	
	if (m_menu && !m_menu->getisUsing()) //If the player exited the menu, to play the game
	{
		//LOAD THE ACTUAL GAME, which means, not the menu
		cout << "Loading the game." << endl;
		m_actualGame = new ActualGame;
		cout << "Game loaded." << endl;

		//THEN DELETE THE MENU
		//Because of the doors and all, it has to be done afterwards
		delete m_menu;
		m_menu = nullptr;
	}
	else if(m_menu) // if the player is in the menu
	{
		runMenu();
	}
	else //if he is playing
	{
		m_actualGame->run();
	}
}