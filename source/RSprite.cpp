#include "RSprite.hpp"

RSprite::RSprite(const std::string& filepath, const float& width, const float& height)
{
	m_texture = new sf::Texture;
	if(!m_texture->loadFromFile(filepath))
	{
		std::cerr << "Could not find texture " << filepath << std::endl;
	}
	m_Rshape = new sf::RectangleShape(sf::Vector2f(width, height));
	m_Rshape->setTexture(m_texture);
}

RSprite::~RSprite()
{
	delete m_texture;
	m_texture = nullptr;

	delete m_Rshape;
	m_Rshape = nullptr;
}

void RSprite::move(const float& x, const float &y) const
{
	m_Rshape->move(x,y);
}

sf::Vector2f RSprite::getPosition() const
{
	return m_Rshape->getPosition();
}

void RSprite::setPosition(const float &x, const float& y) const
{
	m_Rshape->setPosition(x,y);
}