#pragma once
#include <SFML\Graphics.hpp>
#include "Settings.hpp"
//#include "GlobalVars.h"
#include "Basic_funcs.hpp"

class AnimatedSprite : public sf::Sprite
{
public:
	AnimatedSprite(const std::string& pathWithoutExtension);
	//~AnimatedSprite();

	//Returns false when the animation has ended
	bool playAnimation();
	void playAnimationLooped();
	void displayFrame(const unsigned& framenumber);

protected:
	sf::Clock m_timeSpentOnLastFrame;
	
	const unsigned	m_frameWidth;
	const unsigned	m_frameHeight;
	const unsigned	m_totalFrames;
	unsigned		m_currentFrame;

	float m_timeForFrameToStay;

	//Only created and destroyed in the constructor
	//Settings* m_settings;
};