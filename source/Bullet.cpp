#include "Bullet.hpp"

//The imprecision of the pattern is held by the weapon, therefore the pattern
//is randomized in Actualgame.cpp, spawnNewBullet, where the weapon model of the 
//shooter is accessible
Bullet::Bullet(
	sf::Vector2f const& position, 
	Pattern const& pattern,
	BulletModel const& model
	) 
		: m_model(model), 
		m_sprite(model.getTexture()),
		m_pattern(pattern)
{
	m_sprite.setPosition(position);
	m_pattern.moveAllNodesSoFirstOneIsLike(position);
}
void Bullet::followPattern()
{
	m_sprite.move(m_pattern.getMovingVector(m_sprite.getPosition())*m_model.getBulletSpeed()*Var.deltaTime);
}

bool Bullet::hasReachedEndOfPattern()
{
	return m_pattern.reachedEnd();
}

int Bullet::getDamage() const
{
	return m_model.getDamage();
}
