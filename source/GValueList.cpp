#include "GValueList.hpp"

GValueList::GValueList(const short unsigned int& width, const sf::Vector2f& position) : m_width(width), m_position(position), m_hoveredBox(0U)
{
	m_isMainList = true;
	m_hoverBox.setPointCount(4U);
	m_hoveredBox = 0U;

	m_hoverBox.setFillColor(sf::Color(128, 160, 255, 125));
}

GValueList::~GValueList()
{

}

void GValueList::addValue(const sf::String& key, const sf::String& value, const sf::Color& textcolor)
{
	m_keys.push_back(std::pair<sf::Text, sf::Text>(sf::Text(key, Var.normalFont, 30U), sf::Text(value, Var.normalFont, 30U)));
	sort();
	m_numOfChoices++;
}

void GValueList::sort()
{
	short unsigned int i;
	for(i = 0U;i<m_keys.size(); ++i)
	{
		m_keys[i].first.setPosition(m_position.x, m_position.y + i*50);
		m_keys[i].second.setPosition(m_position.x + m_width/2, m_position.y + i*50);
	}
	
	m_hrs.clear();
	m_hrs.resize(m_keys.size() + 1);

	for(i=0U; i<m_hrs.size(); ++i)
	{
		m_hrs[i].setSize(sf::Vector2f(m_width, 2.f));
		m_hrs[i].setFillColor(sf::Color(244U, 207, 245, 200));
		m_hrs[i].setPosition(m_position.x, m_position.y - 10U + i*50);
	}

}

void GValueList::setRelativeHoveredBox(const short int& i)
{

	if(m_hoveredBox + i >= 0 && m_hoveredBox+i < m_numOfChoices)
	{
		m_hoveredBox += i;
	
		m_hoverBox.setPoint(0U, m_hrs[m_hoveredBox].getTransform() * m_hrs[m_hoveredBox].getPoint(3U));
		m_hoverBox.setPoint(1U, m_hrs[m_hoveredBox].getTransform() * m_hrs[m_hoveredBox].getPoint(2U));
		m_hoverBox.setPoint(2U, m_hrs[m_hoveredBox+1].getTransform() * m_hrs[m_hoveredBox+1].getPoint(1U));
		m_hoverBox.setPoint(3U, m_hrs[m_hoveredBox+1].getTransform() * m_hrs[m_hoveredBox+1].getPoint(0U));

	}
}

void GValueList::setAbsoluteHoveredBox(const short unsigned int& i)
{
	m_hoverBox.setPoint(0U, m_hrs[i].getTransform() * m_hrs[i].getPoint(3U));
	m_hoverBox.setPoint(1U, m_hrs[i].getTransform() * m_hrs[i].getPoint(2U));
	m_hoverBox.setPoint(2U, m_hrs[i+1].getTransform() * m_hrs[i+1].getPoint(1U));
	m_hoverBox.setPoint(3U, m_hrs[i+1].getTransform() * m_hrs[i+1].getPoint(0U));

	m_hoveredBox = i;
}

void GValueList::modifyOptionState(const short int index, const std::string newOptionDescription)
{
	m_keys[index].second.setString(newOptionDescription);
}

sf::Text GValueList::getRightColumnValue(size_t const& index)
{
	return m_keys[index].second;
}