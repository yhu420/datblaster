#include "Settings.hpp"


Settings::Settings()
{
	//Absolutely nothing
}

bool Settings::create()
{
	if(filepath.size() == 0)
	{
		cerr << "No file path specified for the settings." << endl;
		return false;
	}
	else
	{
		list.clear();
		in_stream.open(filepath);
		if(!in_stream.is_open())
		{
			cerr << "Could not have opened " << filepath << "." << endl;
			return false;
		}
		else
		{
			while(!in_stream.eof())
			{
				in_stream >> line;
				list.push_back(line);
			}
		}

		in_stream.close();
		
		for(unsigned int i=0; i<list.size(); i++)	//this loops for every word found in the file
		{
			string linestring = list[i];	//list[i] contains the ist word e.g: cat=cute
			string key;						//the key of the ist value e.g cat
			string valueOfKey;				//e.g cute
			bool keyWritten = false;		//if it totally finished to copy the key in key or not
			for(unsigned int j = 0; j<linestring.size(); j++)//this loops for the ist word found in the file letter by letter
			{
				if(linestring[j] == '=' )	//...
				{
					keyWritten = true;		//Then the key is written
				}
				else if(keyWritten)			//If the key has been written
				{
					valueOfKey+=linestring[j];//add another letter to the value
				}
				else
				{
					key+=linestring[j];		//else we did not encounter '=' yet, then it is the key
				}
			}

			//cout << key << "    " << valueOfKey << endl;//couts the key and the value
			//mapOfDoom[key] = SettingsString(valueOfKey); <-- I did not know much about maps and I tried this shame first lol
			mapOfDoom.insert(std::pair<string, SettingsString>(key, SettingsString(valueOfKey)));
		}
	}
	return true;
}

Settings::Settings(string filename)
{
	//cout << "Settings object for " << filename << " created." << endl; // doesnt show for year 2k bug
	filepath = filename;
	create();
}

bool Settings::setFileToParse(string filename)
{
	filepath = filename;
	return create();
}

SettingsString Settings::getSingleValue(const std::string & file, const std::string & key)
{
	Settings tem(file);
	return tem.getValue(key);
}

void Settings::printAllValues() const
{
	for (map<string, SettingsString>::const_iterator it = mapOfDoom.begin(); it!=mapOfDoom.end(); ++it)
		std::cout << it->first << " => " << it->second.toString() << '\n';
}

void Settings::addKey(string key, string value)
{
	//Add it to the mapOfDoom
	mapOfDoom.insert(std::pair<string, SettingsString>(key, SettingsString(value)));
}

bool Settings::removeKey(const string key)
{
	if(mapOfDoom.find(key) == mapOfDoom.end())
	{
		cerr << "Could not find the key to erase" << endl;
		return false;
	}
	else 
	{
		mapOfDoom.erase(key);
		return true;
	}
}

SettingsString Settings::getValue(const string key) const
{
	if(mapOfDoom.find(key) == mapOfDoom.end())
	{
		cerr << "The key " + key + " could not be found in file " + filepath + ". Returning 0.\n" << endl;
		return SettingsString(string("0"));
	}
	else 
	{
		return mapOfDoom.at(key);
	}
}

void Settings::save() const 
{
	//clear the doc and write all values
	ofstream fileStream;
	fileStream.open(filepath);
	if(fileStream.is_open())
	{
		for (map<string, SettingsString>::const_iterator it = mapOfDoom.begin(); it!=mapOfDoom.end(); ++it)
			fileStream << it->first << "=" << it->second.toString() << "\n";
	}
	else
		cerr << "could not re-update values. All the changes done during this session have been lost.\n";
}

void Settings::clearValues()
{
	mapOfDoom.clear();
}

bool Settings::changeValue(const string key, const string value)
{
	if(mapOfDoom.find(key) != mapOfDoom.end())
	{
		removeKey(key);
		addKey(key, value);
		return true;
	}
	else
	{
		cerr << "Failed to find the original value." << endl;
		return false;
	}
}