#include "FullSprite.hpp"

bool FullSprite::loadTexture(const std::string &pathToTexture)
{
	m_texture = new sf::Texture;
	m_sprite = new sf::Sprite;
	if(!m_texture->loadFromFile(pathToTexture))
	{
		std::cerr << "Could not load texture " << pathToTexture << " - Bad file path\n";
		return false;
	}
	else
	{
		m_sprite->setTexture(*m_texture);
	}
	return true;
}

bool FullSprite::changeTexture(const std::string &pathToTexture)
{
	if(exists())
	{
		if(!m_texture->loadFromFile(pathToTexture))//If it failed to load the texture
		{
			std::cerr << "Could not change texture to " << pathToTexture << " - Bad file path" << std::endl;
			return false;
		}
		else//Success
		{
			return true;
		}
	}
	else//The sprite does not exist
	{
		std::cerr << "Cannot change the texture to " << pathToTexture << ": Fullsprite does not even exist." << std::endl;
		return false;
	}
}

FullSprite::FullSprite(const std::string &pathToTexture)
{
	loadTexture(pathToTexture);
}

FullSprite::~FullSprite()
{
	delete m_sprite;
	delete m_texture;

	m_sprite = nullptr;
	m_texture = nullptr;
}