#pragma once
#include <SFML\Audio.hpp>
#include "Bullet.hpp"

class WeaponModel
{
public:
	WeaponModel(
		std::string const& pathToSound, 
		unsigned short const& firerate,
		int const& bulletpatternID,
		int const& bulletID,
		float const& imprecision
		)
			: m_fireRate(firerate),
			m_defaultBulletID(bulletID),
			m_bulletPatternID(bulletpatternID),
			m_imprecision(imprecision)
			{m_soundBuffer.loadFromFile(pathToSound);};

	unsigned short const& getFireRate() const {return m_fireRate;};
	sf::SoundBuffer const& getSoundBuffer() const { return m_soundBuffer; };
	int const& getBulletID() const { return m_defaultBulletID; };
	int const& getPatternID() const { return m_bulletPatternID; };
	float const& getImprecision() const { return m_imprecision; };

protected:
	sf::SoundBuffer			m_soundBuffer;
	
	const unsigned short	m_fireRate;					//Measured in milliseconds
	const int				m_defaultBulletID;
	const unsigned short	m_bulletPatternID;
	const float				m_imprecision;
};