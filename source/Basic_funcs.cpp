#include "Basic_funcs.hpp"

//I consider all those funcs too short for them not to be inline

sf::Vector2f inline normalVector(const sf::Vector2f& vec)
{
	return sf::Vector2f(vec.y, -vec.x);
}

sf::Vector2f inline getVectorTwoPoints(const sf::Vector2f& A, const sf::Vector2f& B)
{
	return sf::Vector2f(B.x - A.x, B.y - A.y);
}

//Point is a point that belongs to the 'line'
//vector is the director vector of the 'line'
float inline getRootLinear(const sf::Vector2f& point, const sf::Vector2f& vector)
{
	//in maths, the eq would be
	//y=vector.x/vector.y * X + point.y-(vecx/vecy)*ptx
	//y=0 <=> vector.x/vector.y * X + point.y-(vecx/vecy)*ptx=0
	//y=0 <=> vector.x/vector.y * X = - point.y+(vecx/vecy)*ptx
	//y=0 <=> X = - (point.y-(vecx/vecy)*ptx)/(vecx/vecy)

	return -point.y * (vector.x / vector.y) + point.x; //Can return infinity, not good for my implementation
}

float inline dotProduct(const sf::Vector2f& A, const sf::Vector2f& B)
{
	return A.x*B.x + A.y*B.y;
}

//I GAVE UP, THESE ARE REMAINS OF UNFINISHED WORK
//This is the minimal version. Rect's rotation has to be untouched, and the global bounds
//must intersect for this function to give correct results. 
//Rect is generally obtained with getGlobalBounds()
bool shapesCollideSAT(const sf::FloatRect& rect, const sf::ConvexShape& shape2)
{
	sf::ConvexShape shape1;
	shape1.setPointCount(4);
	shape1.setPoint(0, sf::Vector2f(rect.left, rect.top));
	shape1.setPoint(1, sf::Vector2f(rect.left + rect.width, rect.top));
	shape1.setPoint(2, sf::Vector2f(rect.left + rect.width, rect.top + rect.height));
	shape1.setPoint(3, sf::Vector2f(rect.left, rect.top + rect.height));

	//http://www.sevenson.com.au/actionscript/sat/

	//There will not be any gap when checking the first shape, as this function is called when the
	//global bounds intersect. Let's only check the second shape.
	//If you don't really get why, I suggest to make a drawing.


	//Need to fix this function!! unfinished work always returns true IMPORTANT /!\

	for (size_t i = 0; i < shape2.getPointCount()-1; i++) //Loop for every side of shape 2
	{
		//There are as many sides as there are points

		cout << "showing side " << i << endl;
		Var.errorMgr.drawArtifactAt(shape2.getTransform().transformPoint(shape2.getPoint(i)));
		Var.errorMgr.drawArtifactAt(shape2.getTransform().transformPoint(shape2.getPoint(i+1)));
		Var.errorMgr.drawLine(
			shape2.getTransform().transformPoint(shape2.getPoint(i)), 
			sf::Vector2f(getRootLinear(shape2.getTransform().transformPoint(shape2.getPoint(i)), getVectorTwoPoints(shape2.getPoint(i), shape2.getPoint(i + 1))),0));
		//does this behave properly? Im not sure I can see only 1 line
		//Draw calls are weird, gotta fix

		
		float min2(0);
		float max2(0);
		for (size_t j = 0; j < shape2.getPointCount(); j++)
		{
			//Let's see what the highest and lowest values are
			//Let's imagine the line following normVec
			//The projection of points i and i+1 will be the same. There could be a way for optimization here
			
			//Here's adeeper explanation of what we are doing here, as I am myself sometimes lost
			//in this section of the code.
			//In the analogy, we are at the part where we are casting light on the second polygon
			//For each side, we are in this j loop trying to find out which point 
			//will be the most on the rght or on the left. 
			//We achieve that using the dot
			//product of the normal vector and the side vector.
			//However, I'm a complete idiot, and do a special method which will soon be in math books.
			//Ill just see what the intersection point is on the absciss axis.
			//http://web.geogebra.org/o/qwg3n5H4
			//EDIT: I can't do that, because there can be no root, in which case I'm screwed.
			
			//Day three: well fuck it, i'll find a collision system by myself, I dont need
			//these SAT shenanigans.


			//That's not so easy to write this code in parctice, so I'll help myself
			//with a drawing you will probably be able to find in a random folder.
			//const float dot = dotProduct(normVec, getVectorTwoPoints(, shape2.getPoint(j)));

			const float root = getRootLinear(shape2.getPoint(j), getVectorTwoPoints(shape2.getPoint(i), shape2.getPoint(i + 1)));
			min2 = std::min(root, min2);
			max2 = std::max(root, max2);
		}
		//Now we know the highest and lowest values for the convex shape
		
		//Let's find what they are for the rectangle.
		float min1(0);
		float max1(0);
		for (size_t j = 0; j < 4; j++)
		{
			//Let's see what the highest and lowest values are
			//Let's imagine the line following normVec
			//The projection of points i and i+1 will be the same, let's say it will be 0.
			const float root = getRootLinear(shape1.getPoint(j), getVectorTwoPoints(shape1.getPoint(i), shape1.getPoint(i + 1)));
			min1 = std::min(root, min1);
			max1 = std::max(root, max1);
		}
		//Now if there is a gap, the two shapes are not touching
		if (max1 < min2 || min1 > max2)
			return false;
	}
	return true;
}

bool shapesCollideCustom(const sf::FloatRect & rect, const sf::ConvexShape & second)
{
	//https://openclassrooms.com/courses/theorie-des-collisions/formes-plus-complexes
	//article is in french, but is really helpful
	//Way easier and faster than the SAT everyone seem to talk about

	const std::size_t pointCount = second.getPointCount();
	for (std::size_t i = 0; i < pointCount; i++)
	{
		//Point A = tab[i];
		sf::Vector2f B;
		if (i == pointCount - 1)  // si c'est le dernier point, on relie au premier
			B = second.getTransform().transformPoint(second.getPoint(0));
		else           // sinon on relie au suivant.
			B = second.getTransform().transformPoint(second.getPoint(i+1));
		
		//vector D = vector AB
		const sf::Vector2f D(getVectorTwoPoints(second.getTransform().transformPoint(second.getPoint(i)), B));
		
		//Vector T = vector AP
		sf::Vector2f T(getVectorTwoPoints(second.getTransform().transformPoint(second.getPoint(i)), sf::Vector2f(rect.left, rect.top)));

		if (D.x*T.y - D.y*T.x < 0)
			return false;  // un point � droite et on arr�te tout.

		//Vector T = vector AP
		T = getVectorTwoPoints(second.getTransform().transformPoint(second.getPoint(i)), sf::Vector2f(rect.left + rect.width, rect.top));

		if (D.x*T.y - D.y*T.x < 0)
			return false;  // un point � droite et on arr�te tout.
		
		//Vector T = vector AP
		T = getVectorTwoPoints(second.getTransform().transformPoint(second.getPoint(i)), sf::Vector2f(rect.left + rect.width, rect.top + rect.height));

		if (D.x*T.y - D.y*T.x < 0)
			return false;  // un point � droite et on arr�te tout.

		//Vector T = vector AP
		T = getVectorTwoPoints(second.getTransform().transformPoint(second.getPoint(i)), sf::Vector2f(rect.left, rect.top + rect.height));

		if (D.x*T.y - D.y*T.x < 0)
			return false;  // un point � droite et on arr�te tout.
	}
	return true;  // si on sort du for, c'est qu'aucun point n'est � gauche, donc c'est bon.
}


void loadMusic()
{
	unsigned short lel = randNumber(0U, 3U); //There are in reality five music tracks, or more (we'll see that in the future), 
											 //one of the first 4 is picked to play. If the new track is the same than last track, 
											 //the new track will be the last one, which can not normally be chosen

	static short unsigned int tracknum = (tracknum == lel) ? ( 4U ) : ( lel ); // TODO Please check if this static var wont harm and does properly its job

	Var.music.stop();
	if(!Var.music.openFromFile(std::string("resources/sound/music/music-"+ std::to_string(tracknum)+".ogg")))
			std::cerr << "Could not find the music file" << std::endl;

	Var.music.play();
}

void loadMusic(const unsigned short int tracknumber)
{
	if(!Var.music.openFromFile(std::string("resources/sound/music/music-"+ std::to_string(tracknumber)+".ogg")))
			std::cerr << "Could not find music-[tracknumber].ogg" << std::endl;

	Var.music.play();
}

void rebindKeys()
{
	Var.goDown = (sf::Keyboard::Key)Var.gSettings.getValue("KB_Down").toInt();
	Var.goUp   = (sf::Keyboard::Key)Var.gSettings.getValue("KB_Up").toInt();
	Var.goRight= (sf::Keyboard::Key)Var.gSettings.getValue("KB_Right").toInt();
	Var.goLeft = (sf::Keyboard::Key)Var.gSettings.getValue("KB_Left").toInt();
	Var.fire   = (sf::Keyboard::Key)Var.gSettings.getValue("KB_Fire").toInt();
	Var.special= (sf::Keyboard::Key)Var.gSettings.getValue("KB_Special").toInt();
}

sf::Vector2u getImageDimensions(const std::string & path)
{
	sf::Image im;
	im.loadFromFile(path);
	return im.getSize();
}
