#pragma once
#include <string>
#ifdef _WIN32
#include <windows.h>
#endif
struct Vars;

//Everything's public, as its main role is to store info often used, simple, not game breaking, and all other excuses
//For some reasons, implementing everything in a .cpp file makes me have to reinclude every file, so i'll just leave this here for now.

class GameConsole
{
public:
	GameConsole();
	bool showFPS;

	void GameConsole::execCommand(const std::string& cmd);
	void prompt();
	void checkInput();
};

GameConsole::GameConsole() :
	showFPS(false)
{

}

void GameConsole::execCommand(const std::string& cmd)
{
	if (cmd == "fps")
	{
		showFPS = !showFPS;
		//Var.gSettings.changeValue("showFPS", to_string(int(showFPS)));
	}
	else if (cmd == "clear")
	{
		cerr << string(30, '\n');
	}
	else if (cmd == "exit")
	{
		exit(EXIT_SUCCESS);
	}
	else if (cmd == "help")
	{
		cout << "Command list:\n-fps\n-clear\n-help\n-exit\n-music\n-fullscreen (experimental)\n-windowed\n";
	}
	else if (cmd == "music")
	{
		Var.wantsMusic = !Var.wantsMusic;
		if (Var.wantsMusic)
			Var.music.play();
		else
			Var.music.stop();
	}
	else if (cmd == "fullscreen")
	{
		//experimental
		Var.toWindow->create(sf::VideoMode(1000, 600, 32), Var.gSettings.getValue("titleName").toString(), sf::Style::None);
		Var.toWindow->setSize(sf::Vector2u(sf::VideoMode::getDesktopMode().width, sf::VideoMode::getDesktopMode().height));
		Var.toWindow->setPosition(sf::Vector2i(0, 0));
	}
	else if (cmd == "windowed")
	{
		Var.toWindow->create(sf::VideoMode(Var.gSettings.getValue("windowedWidth").toInt(), Var.gSettings.getValue("windowedHeight").toInt(), 32), Var.gSettings.getValue("titleName").toString());
	}
	else
	{
		cerr << "Unknown command. Type 'help' for help." << endl;
	}
	cerr << endl;
	Var.deltaTime = 0;
}

void GameConsole::prompt()
{
	cerr << "\n> ";
	string userQuery;
	cin >> userQuery;
	execCommand(userQuery);
}

void GameConsole::checkInput()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::End))
	{
		#ifdef _WIN32
			SetForegroundWindow(GetConsoleWindow());
		#endif // _WIN32
		prompt();
	}
}