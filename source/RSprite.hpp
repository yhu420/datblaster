#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>

class RSprite : public sf::Drawable
{
public:
	RSprite(const std::string&, const float& width, const float &height);
	RSprite();
	~RSprite();

	void move(const float& x, const float &y) const;
	void setPosition(const float &x, const float& y) const;
	void stretch(const float &x, const float &y) const {m_Rshape->setSize(sf::Vector2f(x,y));}
	sf::RectangleShape* getShape() const{return m_Rshape;};

	sf::Vector2f getPosition() const;

	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		target.draw(*m_Rshape);
	}

private:
	sf::RectangleShape *m_Rshape;
	sf::Texture *m_texture;
};