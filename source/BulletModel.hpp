#pragma once
#include <SFML/Graphics.hpp>
#include "Pattern.hpp"

class BulletModel
{
public:
	BulletModel(std::string const& tex, 
		float const& speed, 
		int const& damage) 
			: m_damage(damage),
			m_bulletSpeed(speed)
			{m_tex.loadFromFile(tex);};

	sf::Texture const& getTexture() const {return m_tex;};
	short const& getDamage() const {return m_damage;};
	float const& getBulletSpeed() const {return m_bulletSpeed;};

private:
	sf::Texture						m_tex;
	float const						m_bulletSpeed;
	int const						m_damage;
};