#pragma once
#include <memory>
#include "GameHUD.hpp"
#include "SpaceShip.hpp"
#include "Pattern.hpp"

class ActualGame : public sf::Drawable
{
public:
	ActualGame();
	~ActualGame();

	void run();
	void initModels();
	void spawnNewShip(sf::Vector2f const& position, SpaceShipModel const& model);
	void spawnNewBullet(SpaceShip& shooter);
	void spawnNewDyingCharacter(sf::Vector2f const& position, std::string const& pathNoExt, sf::Texture const& text);

	//This will be polled everytime and spawns the necessary ships for the wave
	//This method will also tell you if the wave can be ended by a boolean
	bool autoShipSpawn();

	void prepareNewWave();

	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		//BG target.draw(
		//SHIPS 
		target.draw(*m_playerShip);
		for(int short unsigned i=0U; i < m_spaceShipHolder.size(); ++i)
		{
			target.draw(*m_spaceShipHolder[i]);
		}
		for(int short unsigned i=0U; i < m_bulletHolder.size(); ++i)
		{
			target.draw(*m_bulletHolder[i]);
		}
		for (int short unsigned i = 0U; i < m_dyingCharacters.size(); ++i)
		{
			target.draw(*m_dyingCharacters[i]);
		}
		//HUD
		target.draw(m_gameHUD);
	}
protected:
	sf::View					m_gameView;
	GameHUD						m_gameHUD;
	sf::Clock					m_cooldownBetweenWaves;

	bool						m_canResumeSpawns; //This is just for optimization, if cooldown > 10
	bool						m_canEndCurrentWave;

	std::vector<BulletModel>	m_bulletModelHolder;
	std::vector<SpaceShipModel>	m_SpaceShipModelHolder;
	std::vector<WeaponModel>	m_weaponModelHolder;

	//The following elements will be drawable

		std::vector<std::unique_ptr<Bullet>>	m_bulletHolder;
		std::vector<std::unique_ptr<SpaceShip>>	m_spaceShipHolder;
		std::vector<std::unique_ptr<SpaceShip>>	m_dyingSpaceShipHolder; //We wont have to check collision for them
		std::unique_ptr<SpaceShip>				m_playerShip;	//Player has its own object, cause it doesnt have to follow any kind of pattern, or automated action
		std::vector<std::unique_ptr<AnimatedSprite>>	m_dyingCharacters; //This is where you put your explosion sprites etc, those are drawn but don't collide or move. At least for now.

	//non drawable game elements

		std::vector<Weapon>						m_weaponHolder;
		std::vector<Pattern>					m_shipPatternHolder;
		std::vector<Pattern>					m_bulletPatternHolder;

	//Wave data, level data

		unsigned short							m_waveNumber;
		unsigned short							m_maxAllowedShipsOnScreenThisWave; //We already have the current number, the size of shipholder
		std::vector<int>						m_remainingSpawn; //the ints are the model ids, we use the last of it as id for next to spawn
		sf::Clock								m_timeSinceLastSpawn;
		float									m_minTimeForNewSpawn;

	//Score, resource data
		unsigned								m_gold;
		unsigned								m_score;

	//Gameplay elements
		const float								m_playerSpeedRight;
		const float								m_playerSpeedLeft;
		const float								m_playerSpeedUp;
		const float								m_playerSpeedDown;
		//const convexshape walkableArea? For now well stick to a simple fake rectangle
		const float								m_playerWalkableAreaMinX;
		const float								m_playerWalkableAreaMaxX;
		const float								m_playerWalkableAreaMinY;
		const float								m_playerWalkableAreaMaxY;
};