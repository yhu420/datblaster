#pragma once
#include "Menu.hpp"
#include "ActualGame.hpp"

class Game : public sf::Drawable
{
public:
	Game();
	void run();

	void runMenu(); // main interface + shop + credits etc. The menu will be manually deleted

	//The pausing system is handeled in main, using events like gainedFocus, Lost focus and everything
	//The pause screen just consists in a freeze, if a player wants to self destruct, i'll TODO make 
	//Something for that, like the escape key

protected:
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		if(m_menu)
		{
			target.draw(*m_menu);
		}
		else //The player is in game
		{
			target.draw(*m_actualGame);
		}
	}

	Menu *m_menu;
	ActualGame *m_actualGame;
};
