#pragma once
#include "Basic_funcs.hpp"
#include "WeaponModel.hpp"

/*

	The use of a weapon is make sure the right bullet with right properties, and to play its own sound.

	From the ship, we have:
		-Position (to know where to initially spawn the bullet)

	From the weapon model, we have:
		-Sprite buffer?
		-Sound buffer
		-Fire rate
		-Bullet model ID

	From the object instance, we can calculate:
		-A new bullet with:
			-Its pattern
			-Its model

*/

class Weapon
{
public:
	Weapon(WeaponModel const& wpnModel) : 
		m_model(wpnModel),
		m_bulletModelID(wpnModel.getBulletID()),
		m_bulletPatternID(wpnModel.getPatternID()),
		m_imprecision(wpnModel.getImprecision())
		{m_sound.setBuffer(m_model.getSoundBuffer());};

	Weapon& operator=(Weapon const other){ Weapon::Weapon(other); return *this; };

	bool readyToFire() const {return m_timeSinceLastShoot.getElapsedTime().asMilliseconds() > m_model.getFireRate();};
	void fakeFire() { m_timeSinceLastShoot.restart(); m_sound.play(); };
	int const& getBulletModelID() const { return m_bulletModelID; };
	int const& getBulletPatternID() const { return m_bulletPatternID; };
	float const& getImprecision() const { return m_imprecision; };

protected:
	WeaponModel const&	m_model;
	sf::Sound			m_sound;				//The sound to play the model's buffer
	sf::Clock			m_timeSinceLastShoot;	//Measured in milliseconds
	int					m_bulletModelID;		//Not const, cause it can change with changing weapon
	int					m_bulletPatternID;		//Idem
	float				m_imprecision;			//same
};