#pragma once
#include <SFML\Graphics.hpp>
#include "FullSprite.hpp"
#include "Basic_funcs.hpp"


class MainMenu : public sf::Drawable
{
public:

	MainMenu();

	void run();
	void shakeCharacter();

	void selectEntry(const short & index);
	void selectEntryAbove();
	void selectEntryUnder();

	unsigned int getEntry() const;

	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		target.draw(m_staticbg);
		target.draw(m_playtxt);
		target.draw(m_shoptxt);
		target.draw(m_optionstxt);
		target.draw(m_abouttxt);
		target.draw(m_exittxt);
		target.draw(m_moneytxt);
		target.draw(m_highscoretxt);
		target.draw(m_shakedCharacter);
	}

protected:
	Settings m_settings;
	void setLitEntry(sf::Text *litOne);
	
	short m_litOne;
	const float m_shakespeed;
	const float m_shakeavgx;
	const float m_shakeavgy;
	const float m_shakemaxdist;

	FullSprite m_staticbg;
	FullSprite m_shakedCharacter;

	sf::Text m_playtxt;
	sf::Text m_shoptxt;
	sf::Text m_optionstxt;
	sf::Text m_abouttxt;
	sf::Text m_exittxt;

	sf::Text m_moneytxt;
	sf::Text m_highscoretxt;

	sf::Text Mother;
	sf::Text Blaster;

};