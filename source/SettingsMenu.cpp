#include "SettingsMenu.hpp"

SettingsMenu::SettingsMenu() : m_background(std::string("resources/images/menu/settingsBG.jpg"), 1000.f, 600.f), m_valueList(800U, sf::Vector2f(100.f, 175.f))
{
	setMainm_optionstxtList();

	//////////////////////////////////////////////////////////////
	m_title.setFont(Var.scorchedFont);
	m_title.setCharacterSize(100U);
	m_title.setPosition(225.f, 25.f);
	m_title.setColor(sf::Color(110, 197, 255, 200));
	m_title.setString(sf::String("Settings"));

	m_goUp = Var.goUp;
	m_goDown = Var.goDown;
	m_goLeft = Var.goLeft;
	m_goRight = Var.goRight;
	m_fire = Var.fire;
	m_special = Var.special;
}

SettingsMenu::~SettingsMenu()
{
	Var.gSettings.changeValue("wantsMusic", std::to_string(m_valueList.getRightColumnValue(1).getString().toAnsiString() == "On" ? 1U : 0U));
	
	cerr << "m_playtxter is gone from SettingsMenu." << std::endl;
	cerr << std::to_string(m_valueList.getRightColumnValue(1).getString().toAnsiString() == "On" ? 1U : 0U);
}

void SettingsMenu::setMainm_optionstxtList()
{
	m_valueList.setMainList(true);
	m_valueList.clear();
	m_valueList.addValue("Controls", "Press enter to configure");
	m_valueList.addValue("Music"   , Var.wantsMusic ? "On" : "Off");
	m_valueList.addValue("Go back?", "Go back.");

	m_valueList.setAbsoluteHoveredBox(0U);
}

void SettingsMenu::setControlsOptionList()
{
	m_valueList.setMainList(false);
	m_valueList.clear();
	m_valueList.addValue("Go up", intKeyToStringKey(Var.gSettings.getValue("KB_Up").toInt()) );
	m_valueList.addValue("Go down" , intKeyToStringKey(Var.gSettings.getValue("KB_Down").toInt()) );
	m_valueList.addValue("Go right" , intKeyToStringKey(Var.gSettings.getValue("KB_Right").toInt()) );
	m_valueList.addValue("Go left", intKeyToStringKey(Var.gSettings.getValue("KB_Left").toInt()) );
	m_valueList.addValue("Shoot ya gunz & shit", intKeyToStringKey(Var.gSettings.getValue("KB_Fire").toInt()) );
	m_valueList.addValue("Special", intKeyToStringKey(Var.gSettings.getValue("KB_Special").toInt()) );
	m_valueList.addValue("Go back & save", "Let's get outta here");
	m_valueList.setAbsoluteHoveredBox(0U);
}

void SettingsMenu::run()
{

}

void SettingsMenu::selectAbsoluteOption(const short unsigned int& i)
{
	m_valueList.setAbsoluteHoveredBox(i);
}

void SettingsMenu::selectRelativeOption(const short int& i)
{
	m_valueList.setRelativeHoveredBox(i);
}

void SettingsMenu::changeRightOptionText(const short int index, const std::string newOptionDescription)
{
	m_valueList.modifyOptionState(index, newOptionDescription);
}

std::string SettingsMenu::intKeyToStringKey(const int& theInt)
{
	if(theInt == 25)
		return "Z";

	else if(theInt == 23)
		return "X";

	else if(theInt == 74)
		return "Down arrow";

	else if(theInt == 71)
		return "Left arrow";

	else if(theInt == 72)
		return "Right arrow";

	else if(theInt == 73)
		return "Up arrow";

	else if(theInt == 22)
		return "W";

	else if(theInt == 0)
		return "A";

	else if(theInt == 18)
		return "S";

	else if(theInt == 3)
		return "D";

	else if(theInt == 16)
		return "Q";

	else if(theInt == -1)
		return "Unknown key LOL";

	else
		return "Custom";
}

void SettingsMenu::saveTheKeyBindings()
{
	Var.goUp = m_goUp;
	Var.goDown = m_goDown;
	Var.goRight = m_goRight;
	Var.goLeft = m_goLeft;

	Var.fire = m_fire;
	Var.special = m_special;

	Var.gSettings.changeValue("KB_Left", std::to_string(m_goLeft));
	Var.gSettings.changeValue("KB_Right", std::to_string(m_goRight));
	Var.gSettings.changeValue("KB_Down", std::to_string(m_goDown));
	Var.gSettings.changeValue("KB_Up", std::to_string(m_goUp));

	Var.gSettings.changeValue("KB_Fire", std::to_string(m_fire));
	Var.gSettings.changeValue("KB_Special", std::to_string(m_special));

	Var.gSettings.save();
}

void SettingsMenu::askKey()
{
	m_askwindow = new AskKeyWindow("Please press the new key");
	Var.toWindow->draw(*m_askwindow);
	Var.toWindow->display();

	while (!m_askwindow->hasAKeyStored())
	{
		m_askwindow->waitForKey();
	}
	//Now the player chose his key, let's change the text of the gvaluelist

	m_valueList.modifyOptionState(m_valueList.getOptionIndex(), intKeyToStringKey((int)m_askwindow->getStoredValue())); //Sets the right text
	
	//The following saves the key to the temporary layout
	switch(m_valueList.getOptionIndex())
	{
	case 0:
		m_goUp = m_askwindow->getStoredValue();
		break;
	case 1:
		m_goDown = m_askwindow->getStoredValue();
		break;
	case 2:
		m_goRight = m_askwindow->getStoredValue();
		break;
	case 3:
		m_goLeft = m_askwindow->getStoredValue();
		break;
	case 4:
		m_fire = m_askwindow->getStoredValue();
		break;
	case 5:
		m_special = m_askwindow->getStoredValue();
		break;
	default:
		std::cerr << "WTF? Askkey in settingsmenu.cpp" << std::endl;
		break;
	}

	delete m_askwindow;
	m_askwindow = nullptr;
}

bool SettingsMenu::noDoubleKeys() const
{
	return 
		m_goUp != m_goLeft &&
		m_goUp != m_goDown &&
		m_goUp != m_goRight &&
		m_goUp != m_fire &&
		m_goUp != m_special &&
		
		m_goLeft != m_goDown &&
		m_goLeft != m_goRight &&
		m_goLeft != m_fire &&
		m_goLeft != m_special &&
		
		m_goDown != m_goRight &&
		m_goDown != m_fire &&
		m_goDown != m_special &&
		
		m_goRight != m_fire &&
		m_goRight != m_special &&
		
		m_fire != m_special;
}