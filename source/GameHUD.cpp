#include "GameHUD.hpp"

GameHUD::GameHUD(unsigned* gold, unsigned* score)
{
	//Health bars
	m_healthBackgroundTex.loadFromFile("resources/images/game/hud/health_bg.png");
	m_healthForegroundTex.loadFromFile("resources/images/game/hud/health_fg.png");

	m_healthBarBackground.setTexture(m_healthBackgroundTex);
	m_healthBarForeground.setTexture(m_healthForegroundTex);

	Settings sett;
	sett.setFileToParse("resources/others/hud.cfg");

	m_healthBarBackground.setPosition(sf::Vector2f(
		sett.getValue("healthBarx").toFloat(),
		sett.getValue("healthBary").toFloat()
	));
	m_healthBarForeground.setPosition(sf::Vector2f(
		sett.getValue("healthBarx").toFloat(),
		sett.getValue("healthBary").toFloat()
	));

	//Score and gold
	m_currentGold.setPosition(sf::Vector2f(
		sett.getValue("moneyx").toFloat(), 
		sett.getValue("moneyy").toFloat()
	));

	m_currentScore.setPosition(sf::Vector2f(
		sett.getValue("scorex").toFloat(),
		sett.getValue("scorey").toFloat()
	));

	m_currentScore.setString("Score: 0");
	m_currentGold.setString("Gold: " + Var.gSettings.getValue("globalMoney").toString());

	m_currentGold.setFont(Var.normalFont);
	m_currentScore.setFont(Var.normalFont);

	m_toMoney = gold;
	m_toScore = score;
}

GameHUD::~GameHUD()
{
	if (m_levelAnnounce)
	{
		delete m_levelAnnounce;
		m_levelAnnounce = nullptr;
	}
}

void GameHUD::drawWaveText(const unsigned short & wavenumber)
{
	m_levelAnnounce = new sf::Text;
	m_levelAnnounce->setPosition(300.f, 250.f);
	m_levelAnnounce->setCharacterSize(60U);
	m_levelAnnounce->setString(sf::String("Prepare for wave " + to_string(wavenumber)));
	m_levelAnnounce->setFont(Var.normalFont);
}

void GameHUD::updateLabels()
{
	m_currentGold.setString("Gold: " + to_string(*m_toMoney));
	m_currentScore.setString("Score: " + to_string(*m_toScore));
}

void GameHUD::removeWaveText()
{
	delete m_levelAnnounce;
	m_levelAnnounce = nullptr;
}
