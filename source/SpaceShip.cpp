#include "SpaceShip.hpp"

SpaceShip::SpaceShip(sf::Vector2f const& position, Weapon const& weapon, Pattern const& pattern, SpaceShipModel const& model)
		:	m_health(model.getStartingDefaultHealth()),
			m_model(model),
			m_hitbox(model.getHitbox()),
			m_primaryWeapon(weapon),
			m_pattern(pattern.shufflePattern(model.getPatternImprecision())),
			m_sprite(model.getSpritePath())
{
	m_sprite.setTexture(model.getTexture());
	m_sprite.setPosition(position);
	m_hitbox.setPosition(position);
	m_hitbox.setFillColor(sf::Color(50,100,50,200));
	m_sprite.displayFrame(0);

	m_sprite.setOrigin(m_sprite.getGlobalBounds().width / 2.f, m_sprite.getGlobalBounds().height / 2.f);
	m_hitbox.setOrigin(m_sprite.getGlobalBounds().width / 2.f, m_sprite.getGlobalBounds().height / 2.f);



	setRotation(-90.f); //The player's ship has setRotation to 90 in the ActualGame constructor
	if (model.getPatternRelativeToSpawn())
		m_pattern.moveAllNodesSoFirstOneIsLike(position);
}

void SpaceShip::move(sf::Vector2f const& xy)
{
	move(xy.x, xy.y);
}

void SpaceShip::move(float const& x, float const& y)
{
	m_sprite.move(x,y);
	m_hitbox.move(x,y);
}

void SpaceShip::fakeFire()
{
	m_primaryWeapon.fakeFire();
}

void SpaceShip::followPattern()
{
	move(m_pattern.getMovingVector(m_sprite.getPosition())*m_model.getSpeed()*Var.deltaTime);
}