#pragma once

#include <SFML/System/Clock.hpp>
#include "Basic_funcs.hpp"
#include "Game.hpp"
#include "GameConsole.hpp"

int main( int argc, char *argv[ ]);

void prepareMiscStuff(sf::RenderWindow &window, sf::Event &even, GameConsole &console);