#pragma once

#include "RoundRect.hpp"
#include "Basic_funcs.hpp"
#include "FullSprite.hpp"

class MenuButton : public RoundRect
{
public:
	MenuButton(sf::Texture* NormalTexture, sf::Texture* hoveredTexture, sf::Texture* clicktex, const sf::Vector2f& position, const sf::Vector2f& dimensions = sf::Vector2f(162,100));
	~MenuButton();
	void hover();
	void unHover();
	void setActivated(const bool& activated);
	void setAttribute(FullSprite* sprite);
	FullSprite* getAttribute() const{ return m_attribute;};

private:
	bool isHovered;
	bool isUsed;

	FullSprite* m_attribute;

	sf::Texture* m_normTex;
	sf::Texture* m_hoverTex;
	sf::Texture* m_clickedTex;
};