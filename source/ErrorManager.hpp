#pragma once

#include <SFML/Audio.hpp>
#include <SFML\Graphics\CircleShape.hpp>
#include <SFML\Graphics\RenderWindow.hpp>
#include <SFML\Window\Event.hpp>
#include <iostream>
#include <chrono>
#include <thread>
#include <random>

class ErrorManager
{
public:
	ErrorManager();
	~ErrorManager();

	void playCantSound();
	void displayImportantConsole(const std::string& message);
	void drawArtifactAt(const sf::Vector2f& position);
	void drawLine(const sf::Vector2f& point, const sf::Vector2f& otherpoint);
	sf::RenderWindow* point_to_window; //I like to break the rules.
	sf::Event* point_to_event; //I like to break the rules.

private:
	sf::Sound m_sound;
	sf::SoundBuffer m_sbuffer;
};