#pragma once

#include "FullSprite.hpp"
#include "Basic_funcs.hpp"
#include "RSprite.hpp"

#include "Shop.hpp"
#include "SettingsMenu.hpp"
#include "CreditsMenu.hpp"
#include "MainMenu.hpp"

#include <string>
#include <chrono>
#include <thread>

class Menu : public sf::Drawable
{
public:
	~Menu();
	Menu();

	void run();
	bool getisUsing() const;

	void closeDoors();
	void openDoors();

	void runMain();
	void runCredits();
	void runSettings();
	void runShop();

	void createMain();
	void createCredits();
	void createSettings();
	void createShop();
	
	void delMain();
	void delCredits();
	void delSettings();
	void delShop();

	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		if(isAtMain)
		{
			target.draw(*m_mainMenu);
		}
		else if(isAtShop)
		{
			target.draw(*m_shop);
		}
		else if(isAtSettings)
		{
			target.draw(*m_settingsMenu);
		}
		else if(isAtCredits)
		{
			target.draw(*m_creditsMenu);
		}
		else
		{
			std::cerr << "Player left menu? - Menu.hpp" << std::endl;
		}
		
		if(bool(rightDoor && leftDoor))
		{
			target.draw(*rightDoor);
			target.draw(*leftDoor);
		}
	}

protected:
	bool isAtMain;
	bool isAtCredits;
	bool isAtShop;
	bool isAtSettings;
	bool m_doorsOpenedOnce;

	bool isUsingTheMenu;


	sf::Sound *m_sound; //The ticking sound while selecting entries
	sf::SoundBuffer *m_sBuffer;

	sf::Sound *m_doorSound;
	sf::SoundBuffer *m_doorsBuffer;

	sf::Clock *timeLastPressed;
	RSprite *leftDoor;
	RSprite *rightDoor;
	
	Shop *m_shop;
	SettingsMenu *m_settingsMenu;
	CreditsMenu *m_creditsMenu;
	MainMenu *m_mainMenu;
};