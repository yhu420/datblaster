#include "main.hpp"

Vars Var;

int main(int argc, char *argv[])
{
	sf::RenderWindow window;
	sf::Event event;
	GameConsole console;
	prepareMiscStuff(window, event, console);

	Game game;
	sf::Clock clock;
	sf::Clock fps;



	unsigned int frames = 0;

    while (window.isOpen())
    {
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
			else if(event.type == event.LostFocus)
			{
				
			}
			else if(event.type == event.GainedFocus)
			{

			}
        }
		Var.deltaTime = clock.restart().asSeconds();
		
		game.run();

        window.clear();
		window.draw(game);
        window.display();

		console.checkInput();

		frames++;
		if(fps.getElapsedTime().asSeconds() >= 1)
		{
			if (console.showFPS)
			{
				std::cout << "FPS: " << frames << std::endl;
			}
			frames = 0U;
			fps.restart();

			if(Var.music.getStatus() != sf::Music::Playing && Var.wantsMusic)
				loadMusic();
		}
    }

    return 0;
}

void prepareMiscStuff(sf::RenderWindow &window, sf::Event &even, GameConsole &console)
{
	Var.toWindow = &window;
	Var.toEvent = &even;
	Var.toConsole = &console;
	Var.gSettings.setFileToParse(std::string("resources/others/config.cfg"));
	window.setFramerateLimit((unsigned)Var.gSettings.getValue(std::string("FPSMax")).toInt());
	Var.wantsMusic = Var.gSettings.getValue("wantsMusic").toBool();
	console.showFPS = Var.gSettings.getValue("showFPS").toBool();
	Var.errorMgr.point_to_window = &window;
	Var.errorMgr.point_to_event = &even;

	rebindKeys();

	//Set the correct resolution for the window.
	//The window has defined by cfg files dimensions
	//When going fullscreen, the user must have the choice to either:
	//1)Keep the ratio, and fill the remaining space with black/plain color
	//This can/should be improved in the future
	//2) stretch to fill the full screen.

	//Correct way to get current res

	//sf::VideoMode::getDesktopMode().height;
	//sf::VideoMode::getDesktopMode().width;

	if (Var.gSettings.getValue("fullscreen").toBool())
		console.execCommand("fullscreen");
	else
		console.execCommand("windowed");

	//Loading fonts

	if (!Var.scorchedFont.loadFromFile(std::string("resources/others/fonts/ScorchedEarth.otf")))
	{
		std::cerr << "Could not load the menu font." << std::endl;
	}
	if (!Var.futureFont.loadFromFile(std::string("resources/others/fonts/Neoterique.ttf")))
	{
		std::cerr << "Could not load the second menu font." << std::endl;
	}
	if (!Var.normalFont.loadFromFile(std::string("resources/others/fonts/Juice.ttf")))
	{
		std::cerr << "Could not load the normal font." << std::endl;
	}
	if (!Var.titleFont.loadFromFile(std::string("resources/others/fonts/MAXIMUMSECURITY.ttf")))
	{
		std::cerr << "Could not load the title font." << std::endl;
	}
	
	if (Var.wantsMusic)
		loadMusic();
}