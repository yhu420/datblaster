#pragma once
#ifndef FSPRITE_HPP
#define FSPRITE_HPP

#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>

class FullSprite : public sf::Drawable //Used to be a struct, that's why it is not, well, traditional
{
public:
	sf::Sprite *m_sprite;
	sf::Texture *m_texture;

	//Also does the constructor
	bool loadTexture(const std::string &pathToTexture);
	bool changeTexture(const std::string &pathToTexture);
	bool exists() const {return m_sprite && m_texture;}

	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		target.draw(*m_sprite);
	}

	FullSprite() {};
	FullSprite(const std::string &pathToTexture);
	~FullSprite();
};

#endif