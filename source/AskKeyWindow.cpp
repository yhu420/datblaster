#include "AskKeyWindow.hpp"

AskKeyWindow::AskKeyWindow(const std::string& text) : m_rect(sf::Vector2f(100.f, 140.f), sf::Vector2f(800.f, 400.f), 20.f, 5, sf::Color::Blue, sf::Color(150, 120, 200), 5.f)
{
	m_texture = new sf::Texture;
	m_texture->loadFromFile("resources/images/menu/askkeyBG.png");
	m_rect.setTexture(m_texture);
	m_rect.setFillColor(sf::Color(200, 200, 200, 225));
	hasStoredValue = false;
	m_message.setCharacterSize(50U);
	m_message.setFont(Var.normalFont);
	m_message.setString(text);
	m_message.setPosition(170.f, 300.f);
	m_message.setColor(sf::Color(240, 50, 50));
}

AskKeyWindow::~AskKeyWindow()
{
	delete m_texture;
	m_texture = nullptr;
}

void AskKeyWindow::waitForKey()
{
	while (Var.toWindow->pollEvent(*Var.toEvent))
	{
		if(Var.toEvent->type == sf::Event::KeyPressed)
		{
			m_storedKey = Var.toEvent->key.code;
			hasStoredValue = true;
		}
	}
}

