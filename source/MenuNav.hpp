#pragma once

#include <SFML/Graphics.hpp>
#include "MenuButton.hpp"
#include <vector>

class MenuNav : public sf::Drawable
{
public:
	MenuNav();
	~MenuNav();

	void addButton(const MenuButton button);
	void giveFocusToShape(const unsigned short int& i);
	void setShapeActivated(const unsigned short& i, const bool& yn) { m_buttons[i].setActivated(yn);};
	void sort();
	void activate();
	void deactivate();

	bool getHaveFocus() const{ return m_hasFocus;};
	unsigned short int getFocusedShape() const{ return m_focusedShape;};

	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		for(short unsigned int i=0U; i<m_buttons.size(); ++i)
		{
			target.draw(m_buttons[i]);
			//no attributes for them
			//Apply color masks to not hovered shapes todo
		}
	}

private:
	short unsigned int m_focusedShape;
	bool m_hasFocus;
	std::vector<MenuButton> m_buttons;
};