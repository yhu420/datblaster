#include "ErrorManager.hpp"

ErrorManager::ErrorManager()
{
	m_sbuffer.loadFromFile(std::string("resources/sound/effects/cant.wav"));
	m_sound.setBuffer(m_sbuffer);
}

ErrorManager::~ErrorManager()
{

}

void ErrorManager::playCantSound()
{
	m_sound.play();
}

void ErrorManager::displayImportantConsole(const std::string& message)
{
	std::cout << message << std::endl;
	std::cout << "Press enter to continue.." << std::endl;
	while (!sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	{
		point_to_window->pollEvent(*point_to_event);
		//point_to_window->display();
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(200));

	/*
	#ifdef _WIN32
	system("pause");
	#else //Linux based only
	system("read"); //Not sure about that
	#endif // _WIN32
	*/
}

void ErrorManager::drawArtifactAt(const sf::Vector2f & position)
{
	std::random_device rd;     // only used once to initialise (seed) engine
	std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
	std::uniform_int_distribution<int> uni(0, 255); // guaranteed unbiased

	sf::CircleShape circ = sf::CircleShape(5.f, 10U);
	circ.setFillColor(sf::Color(uni(rng), uni(rng), uni(rng))); //For a fancy look

	circ.setPosition(position - sf::Vector2f(5.f,5.f));

	point_to_window->draw(circ);
	point_to_window->display();
	displayImportantConsole("A circle was drawn at (" + std::to_string(position.x) + ", " + std::to_string(position.y) + ")\n");
}

void ErrorManager::drawLine(const sf::Vector2f & point, const sf::Vector2f & otherpoint)
{
	sf::VertexArray arr(sf::LinesStrip, 2);
	arr[0].position = point;
	arr[1].position = otherpoint;

	point_to_window->draw(arr);
	point_to_window->display();
	displayImportantConsole("A line was drawn between the two points.");
}
