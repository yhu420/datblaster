#pragma once
#include "SpaceShipModel.hpp"
#include "Pattern.hpp"
#include "AnimatedSprite.hpp"

class SpaceShip : public sf::Drawable
{
public:
	SpaceShip(sf::Vector2f const& position, 
		Weapon const& weapon, 
		Pattern const& pattern, 
		SpaceShipModel const& model);
	~SpaceShip(){};
	
	int getHealth() const {return m_health;};
	AnimatedSprite& getSprite() { return m_sprite; };
	const Pattern& getPattern() const { return m_pattern; };
	const Weapon& getWeapon() const { return m_primaryWeapon; };
	const int& getScoreReward() const { return m_model.getScoreReward(); };
	const int& getMoneyReward() const { return m_model.getMoneyReward(); };
	const sf::Vector2f& getStartPoint() const { return m_model.getStartPoint(); };
	const SpaceShipModel& getModel() const { return m_model; };
	const sf::ConvexShape& getHitbox() const { return m_hitbox; };

	void receiveDamage(const int damage){m_health -= damage;};
	void setPrimaryWeapon(Weapon const newWeapon){m_primaryWeapon = newWeapon;};

	void move(sf::Vector2f const& xy);
	void move(float const& x, float const& y);
	void setRotation(float const& rotation) { m_sprite.setRotation(rotation); m_hitbox.setRotation(rotation); };
	void rotate(float const& rotation) { m_sprite.rotate(rotation); m_hitbox.rotate(rotation); };

	bool canFire() const {return m_primaryWeapon.readyToFire();};
	void fakeFire();
	void followPattern();

	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		target.draw(m_hitbox); //DEV ONLY
		target.draw(m_sprite);
	}

private:
	SpaceShipModel const&	m_model;
	
	Pattern					m_pattern;			//Each ship has its own pattern, because its randomized
	int						m_health;
	sf::ConvexShape			m_hitbox;			//The hitbox
	AnimatedSprite			m_sprite;			//The vessel sprite itself
	Weapon					m_primaryWeapon;	//Every Ship has 1 weapon
};
