#pragma once
#include <SFML/Graphics.hpp>
#include "GlobalVars.h"
#include "RSprite.hpp"
#include "AnimatedSprite.hpp"

class CreditsMenu : public sf::Drawable
{
public:
	CreditsMenu();
	~CreditsMenu();

	void displayCredits();

	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		target.draw(m_bg);
		target.draw(m_esc);
		target.draw(m_creditsTitle);
		/*target.draw(m_title1);
		target.draw(m_title2);
		target.draw(m_title3);
		target.draw(m_title4);
		target.draw(m_title5);
		target.draw(m_title6);
		target.draw(m_title7);
		target.draw(m_paul1);
		target.draw(m_robin);
		target.draw(m_paul2);
		target.draw(m_paul3);
		target.draw(m_paul4);
		target.draw(m_paul5);
		target.draw(m_paul6);*/
	}

private:
	sf::Text m_creditsTitle;

	std::vector<sf::Text> m_normalTexts;
	std::vector<sf::Text> m_titleTexts;
	std::vector<AnimatedSprite> m_sprites; // just for fun

	/*sf::Text m_title1;
	sf::Text m_title2;
	sf::Text m_title3;
	sf::Text m_title4;
	sf::Text m_title5;
	sf::Text m_title6;
	sf::Text m_title7;*/

	/*sf::Text m_robin;
	sf::Text m_paul1;
	sf::Text m_paul2;
	sf::Text m_paul3;
	sf::Text m_paul4;
	sf::Text m_paul5;
	sf::Text m_paul6;*/

	RSprite m_esc;
	RSprite m_bg;
};