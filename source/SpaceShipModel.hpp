#pragma once
#include "Weapon.hpp"

class SpaceShipModel
{
public:
	SpaceShipModel(std::string const& texturePath, 
			sf::ConvexShape const& hitbox, 
			sf::Vector2f const bulletStart,
			int const& healthdefault, 
			float const& speed,
			int const& patternID,
			int const& weaponID,
			int const& scoreReward,
			int const& moneyReward,
			float const& patternImprecision,
			bool const& patternRelativeToSPawnPoint,
			std::string const& defaultDeathTexture,
			std::string const& defaultHitTexture
			) : 
		m_defaultHitbox(hitbox), 
		m_speed(speed), 
		m_relativeBulletStartPoint(bulletStart),
		m_defaultHealthLevel(healthdefault),
		m_patternID(patternID),
		m_weaponID(weaponID),
		m_scoreReward(scoreReward),
		m_moneyReward(moneyReward),
		m_sprPathNoExt(texturePath),
		m_sprDyingPathNoExt(defaultDeathTexture),
		m_sprHitPathNoExt(defaultHitTexture),
		m_patternImprecision(patternImprecision),
		m_patternRelativeToSpawnPosition(patternRelativeToSPawnPoint)
	{ 
			m_texture.loadFromFile(texturePath + ".png"); 
			m_hitTexture.loadFromFile(defaultHitTexture + ".png");
			m_deathTexture.loadFromFile(defaultDeathTexture + ".png");
	};

	sf::Texture	const&		getTexture() const { return m_texture; };
	sf::Texture	const&		getHitTexture() const { return m_hitTexture; };
	sf::Texture	const&		getDeathTexture	() const { return m_deathTexture; };

	sf::ConvexShape	const	getHitbox() const { return m_defaultHitbox; };
	sf::Vector2f const&		getStartPoint() const { return m_relativeBulletStartPoint; };

	int const&				getStartingDefaultHealth() const { return m_defaultHealthLevel; };
	float const&			getSpeed() const { return m_speed; };
	int const&				getPatternID() const { return m_patternID; }
	int const&				getWeaponID() const { return m_weaponID; }
	int const&				getScoreReward() const { return m_scoreReward; }
	int const&				getMoneyReward() const { return m_moneyReward; }
	float const&			getPatternImprecision() const { return m_patternImprecision; }
	std::string const&		getSpritePath() const { return m_sprPathNoExt; }
	std::string const&		getDyingSpritePath() const { return m_sprDyingPathNoExt; }
	std::string const&		getHitSpritePath() const { return m_sprHitPathNoExt; }
	bool const&				getPatternRelativeToSpawn() const { return m_patternRelativeToSpawnPosition; }

protected:
	sf::Texture	m_texture;
	sf::Texture m_hitTexture;
	sf::Texture m_deathTexture;

	sf::ConvexShape m_defaultHitbox;
	const sf::Vector2f m_relativeBulletStartPoint;

	const int m_defaultHealthLevel;
	const float m_speed;
	const int m_patternID;
	const int m_weaponID;
	const int m_scoreReward;
	const int m_moneyReward;
	
	const std::string m_sprPathNoExt;
	const std::string m_sprDyingPathNoExt;
	const std::string m_sprHitPathNoExt;

	const float m_patternImprecision;
	const bool m_patternRelativeToSpawnPosition;
};
