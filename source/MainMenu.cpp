#include "MainMenu.hpp"

MainMenu::MainMenu() :
	m_staticbg("resources/images/menu/bg.bmp"),
	m_shakedCharacter("resources/images/menu/mainChara.png"),
	m_litOne(0),
	m_settings("resources/others/menu.cfg"),
	m_shakespeed(m_settings.getValue("CharacterShakingSpeed").toFloat()),
	m_shakeavgx(m_settings.getValue("CharacterAveragePosX").toFloat()),
	m_shakeavgy(m_settings.getValue("CharacterAveragePosY").toFloat()),
	m_shakemaxdist(m_settings.getValue("CharacterMaxShakeDist").toFloat())
{
	std::cout << "Welcome back to the main menu!" << std::endl;

	m_staticbg.m_sprite->setScale(
		Var.gSettings.getValue("windowedWidth").toFloat() / m_staticbg.m_sprite->getLocalBounds().width,
		Var.gSettings.getValue("windowedHeight").toFloat() / m_staticbg.m_sprite->getLocalBounds().height);

	m_playtxt.setFont(Var.futureFont);
	m_playtxt.setCharacterSize(60);
		
	m_shoptxt = m_playtxt;
	m_optionstxt = m_playtxt;
	m_abouttxt = m_playtxt;
	m_exittxt = m_playtxt;

	m_playtxt.setPosition	(100, 300); //TOMOD
	m_shoptxt.setPosition	(100, 350); //TOMOD
	m_optionstxt.setPosition(100, 400); //TOMOD
	m_abouttxt.setPosition	(100, 450); //TOMOD
	m_exittxt.setPosition	(100, 500); //TOMOD

	//Todo, set starting shakedchara position

	m_playtxt.setString(sf::String		("Play")); //TOMOD
	m_shoptxt.setString(sf::String		("Shop")); //TOMOD
	m_optionstxt.setString(sf::String	("Options")); //TOMOD
	m_abouttxt.setString(sf::String		("About")); //TOMOD
	m_exittxt.setString(sf::String		("Exit")); //TOMOD

	m_moneytxt.setFont(Var.normalFont);
	m_highscoretxt.setFont(Var.normalFont);

	m_moneytxt.setString(Var.gSettings.getValue("currencyName").toString() + ": " + Var.gSettings.getValue("globalMoney").toString());
	m_highscoretxt.setString("Highscore: " + Var.gSettings.getValue("highscore").toString());

	m_moneytxt.setColor(sf::Color::White);
	m_highscoretxt.setColor(sf::Color::White);

	m_moneytxt.setPosition(400, 20);
	m_highscoretxt.setPosition(700, 20);

	selectEntry(0);
}

void MainMenu::run()
{
	shakeCharacter();
}

void MainMenu::shakeCharacter()
{
	//If x well placed
	if (m_shakedCharacter.m_sprite->getPosition().x > m_shakeavgx - m_shakemaxdist &&
		m_shakedCharacter.m_sprite->getPosition().x < m_shakeavgx + m_shakemaxdist)
		m_shakedCharacter.m_sprite->move(randNumber(-m_shakespeed, m_shakespeed) * Var.deltaTime, 0.f);
	
	//If y well placed
	if (m_shakedCharacter.m_sprite->getPosition().y > m_shakeavgy - m_shakemaxdist && 
		m_shakedCharacter.m_sprite->getPosition().y < m_shakeavgy + m_shakemaxdist)
		m_shakedCharacter.m_sprite->move(0.f, randNumber(-m_shakespeed, m_shakespeed) * Var.deltaTime);

	if (m_shakedCharacter.m_sprite->getPosition().x >= m_shakeavgx + m_shakemaxdist)
		m_shakedCharacter.m_sprite->move(-m_shakespeed * Var.deltaTime, 0.f);

	else if (m_shakedCharacter.m_sprite->getPosition().x <= m_shakeavgx - m_shakemaxdist)
		m_shakedCharacter.m_sprite->move(m_shakespeed * Var.deltaTime, 0.f);

	if (m_shakedCharacter.m_sprite->getPosition().y >= m_shakeavgy + m_shakemaxdist)
		m_shakedCharacter.m_sprite->move(0.f, -m_shakespeed * Var.deltaTime);

	else if (m_shakedCharacter.m_sprite->getPosition().y < m_shakeavgy - m_shakemaxdist)
		m_shakedCharacter.m_sprite->move(0.f, m_shakespeed * Var.deltaTime);
}

void MainMenu::selectEntry(const short & index)
{
	switch (index)
	{
	case 0:
		setLitEntry(&m_playtxt);
		m_litOne = 0;
		break;
	case 1:
		setLitEntry(&m_shoptxt);
		m_litOne = 1;
		break;
	case 2:
		setLitEntry(&m_optionstxt);
		m_litOne = 2;
		break;
	case 3:
		setLitEntry(&m_abouttxt);
		m_litOne = 3;
		break;
	case 4:
		setLitEntry(&m_exittxt);
		m_litOne = 4;
		break;
	default:
		break;
	}
}

void MainMenu::selectEntryAbove()
{
	selectEntry(m_litOne - 1);
}

void MainMenu::selectEntryUnder()
{
	selectEntry(m_litOne + 1);
}

unsigned int MainMenu::getEntry() const
{
	return m_litOne;
}

void MainMenu::setLitEntry(sf::Text *litOne)
{
	m_shoptxt.setColor(		sf::Color(255, 255, 255, 100));
	m_abouttxt.setColor(	sf::Color(255, 255, 255, 100));
	m_exittxt.setColor(		sf::Color(255, 255, 255, 100));
	m_optionstxt.setColor(	sf::Color(255, 255, 255, 100));
	m_playtxt.setColor(		sf::Color(255, 255, 255, 100));

	litOne->setColor(sf::Color(255, 255, 255, 255));
}
