#include "Shop.hpp"

#pragma warning( disable : 4244 )

Shop::Shop() : m_hoveredShape(0U)
{
	//preparing the title
	m_shopTitle.setString(sf::String("SHOP"));
	m_shopTitle.setFont(Var.scorchedFont);
	m_shopTitle.setCharacterSize(100U);
	m_shopTitle.setPosition(350.f, 25.f);
	m_shopTitle.setColor(sf::Color(153, 50, 255, 200));
	//title done

	if(!m_butTexNormal) //Button textures
	{
		m_butTexNormal = new sf::Texture;
		m_butTexActivated = new sf::Texture;
		m_butTexHovered = new sf::Texture;

		m_butTexNormal->loadFromFile(std::string("resources/images/menu/butBG.gif"));
		m_butTexHovered->loadFromFile(std::string("resources/images/menu/butHover.gif"));
		m_butTexActivated->loadFromFile(std::string("resources/images/menu/butAct.gif"));
	}
	else
	{
		std::cerr << "shuld not be displayed" << std::endl;
	}
	if(!m_navBackTex) //Nav textures
	{
		m_navBackTex = new sf::Texture;
		m_navExtraTex = new sf::Texture;
		m_navGunzTex = new sf::Texture;
		m_navHullTex = new sf::Texture;

		m_navBackTex->loadFromFile(std::string("resources/images/menu/navBack.gif"));
		m_navExtraTex->loadFromFile(std::string("resources/images/menu/navExtra.gif"));
		m_navGunzTex->loadFromFile(std::string("resources/images/menu/navGunz.gif"));
		m_navHullTex->loadFromFile(std::string("resources/images/menu/navHull.gif"));
	}
	else
	{
		std::cerr << "Shd not be displayed either shop.cpp" << std::endl;
	}
	m_background = new RSprite(std::string("resources/images/menu/shopBG.jpg"),1000.f,600.f);

	m_nav= new MenuNav;
	m_nav->addButton(MenuButton(m_navExtraTex, m_navExtraTex, m_navExtraTex, sf::Vector2f(0.f,0.f)));
	m_nav->addButton(MenuButton(m_navGunzTex, m_navGunzTex, m_navGunzTex, sf::Vector2f(0.f,0.f)));
	m_nav->addButton(MenuButton(m_navHullTex, m_navHullTex, m_navHullTex, sf::Vector2f(0.f,0.f)));
	m_nav->addButton(MenuButton(m_navBackTex, m_navBackTex, m_navBackTex, sf::Vector2f(0.f,0.f)));
	m_nav->sort();
	m_nav->giveFocusToShape(0U);
	//nav initialized

	//initializing buttons
	setButtonCount(10U); //To change
	//TODO Check activated buttons through settings class
	//Buttons done
}

void Shop::setButtonCount(const int short unsigned& count)
{
	//Size of butonlist must be ++ than count, just to make sure there's no shit

	size_t i=m_buttonList.size();
	
	if(count < m_buttonList.size()+1)
	{
		while(!!m_buttonList[i])
		{
			delete m_buttonList[i];
			m_buttonList[i] = nullptr;
			i--;
		}
	}
	
	m_buttonList.resize(count+1);

	for(i=0U; i<count; i++)
	{
		if(!!m_buttonList[i])
		{
			//m_buttonList[i]->setPosition(187+187*(i%3), 175+(125*int(i/4)));
		}
		else
		{
			m_buttonList[i]=new MenuButton(m_butTexNormal, m_butTexHovered, m_butTexActivated, sf::Vector2f(187+205*(i%4), 175+(150*int(i/4))));
		}
	}
}

void Shop::setRelativeHoveredShape(const short int& shapenum)
{
	if(m_buttonList.size()-1U > unsigned(m_hoveredShape+shapenum) && m_hoveredShape+shapenum >= 0)
	{
		m_buttonList[m_hoveredShape]->unHover();
		m_buttonList[m_hoveredShape+shapenum]->hover();

		m_hoveredShape += shapenum;
	}
}

void Shop::setAbsoluteHoveredShape(const short unsigned int& shapenum)
{
	if(m_buttonList.size() > shapenum && shapenum >= 0)
	{
		m_buttonList[m_hoveredShape]->unHover();
		m_buttonList[shapenum]->hover();

		m_hoveredShape = shapenum;
	}
}

Shop::~Shop()
{

}


#pragma warning( default : 4244 )