#pragma once
#include "ErrorManager.hpp" //includes SFML audio + std
#include "Settings.hpp" //Only incudes std objects
#include <SFML\Graphics\RenderWindow.hpp>
#include <SFML\Graphics\Font.hpp>
#include <SFML\Window\Keyboard.hpp>
class GameConsole;


struct Vars
{
	bool wantsMusic;
	float deltaTime;

	GameConsole *toConsole;
	sf::RenderWindow *toWindow;
	sf::Event *toEvent;

	Settings gSettings;
	ErrorManager errorMgr;

	sf::Font scorchedFont;
	sf::Font futureFont;
	sf::Font normalFont;
	sf::Font titleFont;

	sf::Music music;
	
	sf::Keyboard::Key goUp;
	sf::Keyboard::Key goDown;
	sf::Keyboard::Key goRight;
	sf::Keyboard::Key goLeft;
	sf::Keyboard::Key fire;
	sf::Keyboard::Key special;
};

extern Vars Var;