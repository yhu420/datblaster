#include "RoundRect.hpp"

#define PI 3.14159265359f

RoundRect::RoundRect(const sf::Vector2f& position, const sf::Vector2f& dimensions,const float& borderRadius,  const int unsigned& pointCount, const sf::Color& fillcolor, const sf::Color& outColor, const float& outThickness)
{
	unsigned int i;
	m_pointCount = pointCount*4U;
	for(i=0;i<pointCount;++i) //ANGLE 1
	{
		m_points.push_back(sf::Vector2f(borderRadius - borderRadius*std::cosf((i*PI)/((pointCount-1)*2)),borderRadius - borderRadius*std::sinf((i*PI)/((pointCount-1)*2))));
		//std::cout << "PosX of point " << i << ": " << m_shape.getPoint(i).x << std::endl;
		//std::cout << "PosY of point " << i << ": " << m_shape.getPoint(i).y << std::endl;
	}
	for(i=0;i<pointCount;++i)
	{
		m_points.push_back(sf::Vector2f(-borderRadius+ dimensions.x + borderRadius*std::sinf((i*PI)/((pointCount-1)*2)),borderRadius- borderRadius*std::cosf((i*PI)/((pointCount-1)*2))));
		//std::cout << "PosX of point " << i+pointCount << ": " << m_shape.getPoint(i+pointCount).x << std::endl;
		//std::cout << "PosY of point " << i+pointCount << ": " << m_shape.getPoint(i+pointCount).y << std::endl;
	}
	for(i=0;i<pointCount;++i)
	{//was with sin sin
		m_points.push_back(sf::Vector2f(dimensions.x -borderRadius+ borderRadius*std::cosf((i*PI)/((pointCount-1)*2)),dimensions.y - borderRadius + borderRadius*std::sinf((i*PI)/((pointCount-1)*2))));
		//std::cout << "PosX of point " << i+pointCount*2 << ": " << m_shape.getPoint(i+pointCount*2).x << std::endl;
		//std::cout << "PosY of point " << i+pointCount*2 << ": " << m_shape.getPoint(i+pointCount*2).y << std::endl;
	}
	for(i=0;i<pointCount;++i)
	{
		m_points.push_back(sf::Vector2f(borderRadius - borderRadius*std::sinf((i*PI)/((pointCount-1)*2)),-borderRadius+ dimensions.y + borderRadius*std::cosf((i*PI)/((pointCount-1)*2))));
		//std::cout << "PosX of point " << i+pointCount*3 << ": " << m_shape.getPoint(i+pointCount*3).x << std::endl;
		//std::cout << "PosY of point " << i+pointCount*3 << ": " << m_shape.getPoint(i+pointCount*3).y << std::endl;
	}
	
	setPosition(position);
	setOutlineThickness(outThickness);
	setFillColor(fillcolor);
	setOutlineColor(outColor);
	update();
}

RoundRect::RoundRect(const short unsigned int& cornerToRound, const sf::Vector2f& position, const sf::Vector2f& dimensions,const float& borderRadius, const int unsigned& pointCount, const sf::Color& fillcolor, const sf::Color& outColor, const float& outThickness)
{
	unsigned int i;
	m_pointCount = 3U+pointCount;
	switch(cornerToRound)
	{
	case 1U:
		for(i=0;i<pointCount;++i) //ANGLE 1
		{
			m_points.push_back(sf::Vector2f(borderRadius - borderRadius*std::cosf((i*PI)/((pointCount-1)*2)),borderRadius - borderRadius*std::sinf((i*PI)/((pointCount-1)*2))));
			//std::cout << "PosX of point " << i << ": " << m_shape.getPoint(i).x << std::endl;
			//std::cout << "PosY of point " << i << ": " << m_shape.getPoint(i).y << std::endl;
		}
		m_points.push_back(sf::Vector2f(dimensions.x, 0.f));//adds top right
		m_points.push_back(sf::Vector2f(dimensions.x, position.y + dimensions.y));//adds bottom right
		m_points.push_back(sf::Vector2f(0.f, dimensions.y));//adds the bottom left
		break;

	case 2U:
		m_points.push_back(sf::Vector2f(0.f, 0.f));//Adds top left
		for(i=0;i<pointCount;++i) //ANGLE 2
		{
			m_points.push_back(sf::Vector2f(-borderRadius+ dimensions.x + borderRadius*std::sinf((i*PI)/((pointCount-1)*2)),+borderRadius- borderRadius*std::cosf((i*PI)/((pointCount-1)*2))));
			//std::cout << "PosX of point " << i+pointCount << ": " << m_shape.getPoint(i+pointCount).x << std::endl;
			//std::cout << "PosY of point " << i+pointCount << ": " << m_shape.getPoint(i+pointCount).y << std::endl;
		}
		m_points.push_back(sf::Vector2f(dimensions.x, dimensions.y));//adds bottom right
		m_points.push_back(sf::Vector2f(0.f, dimensions.y));//adds the bottom left

		break;

	case 3U:
		m_points.push_back(sf::Vector2f(0.f, 0.f));//Adds top left
		m_points.push_back(sf::Vector2f(dimensions.x, 0.f));//adds top right
		for(i=0;i<pointCount;++i) //ANGLE 3
		{//was with sin sin
			m_points.push_back(sf::Vector2f(dimensions.x -borderRadius+ borderRadius*std::cosf((i*PI)/((pointCount-1)*2)),dimensions.y - borderRadius + borderRadius*std::sinf((i*PI)/((pointCount-1)*2))));
			//std::cout << "PosX of point " << i+pointCount*2 << ": " << m_shape.getPoint(i+pointCount*2).x << std::endl;
			//std::cout << "PosY of point " << i+pointCount*2 << ": " << m_shape.getPoint(i+pointCount*2).y << std::endl;
		}
		m_points.push_back(sf::Vector2f(0.f, dimensions.y));//adds the bottom left
		break;

	case 4U:
		m_points.push_back(sf::Vector2f(0.f, 0.f));//Adds top left
		m_points.push_back(sf::Vector2f(dimensions.x, 0.f));//adds top right
		m_points.push_back(sf::Vector2f(dimensions.x, dimensions.y));//adds bottom right
		for(i=0;i<pointCount;++i) //ANGLE 4
		{
			m_points.push_back(sf::Vector2f(borderRadius - borderRadius*std::sinf((i*PI)/((pointCount-1)*2)), -borderRadius+ dimensions.y + borderRadius*std::cosf((i*PI)/((pointCount-1)*2))));
			//std::cout << "PosX of point " << i+pointCount*3 << ": " << m_shape.getPoint(i+pointCount*3).x << std::endl;
			//std::cout << "PosY of point " << i+pointCount*3 << ": " << m_shape.getPoint(i+pointCount*3).y << std::endl;
		}

		break;
	}
	
	setPosition(position);
	setOutlineThickness(outThickness);
	setFillColor(fillcolor);
	setOutlineColor(outColor);
	update();
}

RoundRect::RoundRect(const sf::Vector2f& position, const sf::Vector2f& dimensions,const float& borderRadius, const int unsigned& pointCount, const sf::Texture* texture, const sf::Color& outColor, const float& outThickness)
{
	unsigned int i;
	m_pointCount = pointCount*4U;
	for(i=0;i<pointCount;++i) //ANGLE 1
	{
		m_points.push_back(sf::Vector2f(borderRadius - borderRadius*std::cosf((i*PI)/((pointCount-1)*2)),borderRadius - borderRadius*std::sinf((i*PI)/((pointCount-1)*2))));
		//std::cout << "PosX of point " << i << ": " << m_shape.getPoint(i).x << std::endl;
		//std::cout << "PosY of point " << i << ": " << m_shape.getPoint(i).y << std::endl;
	}
	for(i=0;i<pointCount;++i)
	{
		m_points.push_back(sf::Vector2f(-borderRadius+ dimensions.x + borderRadius*std::sinf((i*PI)/((pointCount-1)*2)),borderRadius- borderRadius*std::cosf((i*PI)/((pointCount-1)*2))));
		//std::cout << "PosX of point " << i+pointCount << ": " << m_shape.getPoint(i+pointCount).x << std::endl;
		//std::cout << "PosY of point " << i+pointCount << ": " << m_shape.getPoint(i+pointCount).y << std::endl;
	}
	for(i=0;i<pointCount;++i)
	{//was with sin sin
		m_points.push_back(sf::Vector2f(dimensions.x -borderRadius+ borderRadius*std::cosf((i*PI)/((pointCount-1)*2)),dimensions.y - borderRadius + borderRadius*std::sinf((i*PI)/((pointCount-1)*2))));
		//std::cout << "PosX of point " << i+pointCount*2 << ": " << m_shape.getPoint(i+pointCount*2).x << std::endl;
		//std::cout << "PosY of point " << i+pointCount*2 << ": " << m_shape.getPoint(i+pointCount*2).y << std::endl;
	}
	for(i=0;i<pointCount;++i)
	{
		m_points.push_back(sf::Vector2f(borderRadius - borderRadius*std::sinf((i*PI)/((pointCount-1)*2)),-borderRadius+ dimensions.y + borderRadius*std::cosf((i*PI)/((pointCount-1)*2))));
		//std::cout << "PosX of point " << i+pointCount*3 << ": " << m_shape.getPoint(i+pointCount*3).x << std::endl;
		//std::cout << "PosY of point " << i+pointCount*3 << ": " << m_shape.getPoint(i+pointCount*3).y << std::endl;
	}
	
	setPosition(position);
	setOutlineThickness(outThickness);
	setTexture(texture);
	setOutlineColor(outColor);
	update();
}

RoundRect::~RoundRect()
{
	m_points.clear();
}