#pragma once

#include "RoundRect.hpp"
#include "GlobalVars.h"

class AskKeyWindow : public sf::Drawable
{
public:
	AskKeyWindow(const std::string& text);
	~AskKeyWindow();
	void waitForKey();
	bool hasAKeyStored() const {return hasStoredValue;};
	sf::Keyboard::Key getStoredValue() const {return m_storedKey;};
	
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		target.draw(m_rect, states);
		target.draw(m_message, states);
	}
	
private:
	RoundRect m_rect;
	sf::Texture* m_texture;
	sf::Text m_message;
	bool hasStoredValue;
	sf::Keyboard::Key m_storedKey;
};