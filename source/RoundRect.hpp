#pragma once

#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>

//Position then dimension
class RoundRect : public sf::Shape
{
public:
	RoundRect(const sf::Vector2f& position, const sf::Vector2f& dimensions,const float& borderRadius, const int unsigned& pointCount, const sf::Color& fillcolor, const sf::Color& outColor = sf::Color::Transparent, const float& outThickness = 0.f);
	 
	//1-> top left; 2->top right; 3->bottom right; 4->bottom left; (clock rotation)
	RoundRect(const short unsigned int& cornerToRound, const sf::Vector2f& position, const sf::Vector2f& dimensions,const float& borderRadius, const int unsigned& pointCount, const sf::Color& fillcolor, const sf::Color& outColor = sf::Color::Transparent, const float& outThickness = 0.f);
	

	RoundRect(const sf::Vector2f& position, const sf::Vector2f& dimensions,const float& borderRadius, const int unsigned& pointCount, const sf::Texture* texture, const sf::Color& outColor = sf::Color::Transparent, const float& outThickness = 0.f);
	
	
	~RoundRect();

	virtual unsigned int getPointCount() const
	{
		return m_pointCount;
	}
	virtual sf::Vector2f getPoint (unsigned int index) const
	{
		return m_points[index];
	}
private:
	unsigned int m_pointCount;
	std::vector<sf::Vector2f> m_points;
};