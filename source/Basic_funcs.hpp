#pragma once 

#include <random>
#include "GlobalVars.h"
#include <SFML\Graphics\ConvexShape.hpp>

template<typename T>
T randNumber(const T &min,const T &max);

template<typename T>
void arrayDisplay(std::vector<T> const& todisp);

void loadMusic();
void rebindKeys();
sf::Vector2u getImageDimensions(const std::string& path);
sf::Vector2f inline normalVector(const sf::Vector2f& vec);
sf::Vector2f inline getVectorTwoPoints(const sf::Vector2f& A, const sf::Vector2f& B);
float inline dotProduct(const sf::Vector2f& A, const sf::Vector2f& B);
bool shapesCollideSAT(const sf::FloatRect& rect, const sf::ConvexShape& second);
bool shapesCollideCustom(const sf::FloatRect& rect, const sf::ConvexShape& second);

//I should implement a simpler test for collisions, for example, if
//a vertex is inside a polygon, if we draw a long straight line to 
//the right, it should cross a 'impair' (odd or even, I suck at english)
//number of sides of the polygon.
//http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
//is a good place to take a look and wonder about life.

// TEMPLATE FUNCS MUST BE IMPLEMENTED IN THE HEADER FILE !!!!
// I can't get them working any other way :'(

template<typename T>
T randNumber(const T& min, const T& max)
{
	static std::random_device rd; // static so it doesnt have to be instanced every time
	return min + ((rd() % 100) / 100.f) * (max - min);
}

template<typename T>
void arrayDisplay(std::vector<T> const& todisp)
{
	std::cerr << "--------------------------------------- Size: " << todisp.size() << std::endl;
	for (size_t i = 0; i < todisp.size(); i++)
	{
		std::cerr << "| value[" << i << "]   |   " << todisp[i] << "   |" << std::endl;
	}
	std::cerr << "-----------------------------------------------" << std::endl;
}