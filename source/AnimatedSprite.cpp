#include "AnimatedSprite.hpp"

AnimatedSprite::AnimatedSprite(const std::string & pathWithoutExtension):
	m_frameWidth(Settings::getSingleValue(pathWithoutExtension + ".cfg","FrameWidth").toInt()),
	m_timeForFrameToStay(Settings::getSingleValue(pathWithoutExtension + ".cfg", "FrameTime").toFloat()),
	m_frameHeight(getImageDimensions(pathWithoutExtension + ".png").y),
	m_currentFrame(0),
	m_totalFrames(getImageDimensions(pathWithoutExtension + ".png").x/m_frameWidth)
{
	if (m_frameWidth == 0 || 
		getImageDimensions(pathWithoutExtension + ".png").x % m_frameWidth != 0)
	{
		Var.errorMgr.playCantSound();
		Var.errorMgr.displayImportantConsole("An error occurred loading animated sprites, watch for file " + pathWithoutExtension);
		exit(EXIT_SUCCESS);
	}
	displayFrame(0);
}

bool AnimatedSprite::playAnimation()
{
	if (m_timeSpentOnLastFrame.getElapsedTime().asSeconds() >= m_timeForFrameToStay)
	{
		if (m_currentFrame++ + 1 >= m_totalFrames)
		{
			return false;
		}
		displayFrame(m_currentFrame);
	}
	return true;
}

void AnimatedSprite::playAnimationLooped()
{
	if (!playAnimation())
	{
		displayFrame(0U);
	}
}

void AnimatedSprite::displayFrame(const unsigned & framenumber)
{
	if (framenumber >= m_totalFrames)
	{
		cout << "WARNING: Sprite requested frame exceeds the max frame number" << endl;
		return;
	}
	m_currentFrame = framenumber;
	setTextureRect(sf::IntRect(
		framenumber*m_frameWidth,
		0,
		m_frameWidth,
		m_frameHeight
	));
	m_timeSpentOnLastFrame.restart();
}
