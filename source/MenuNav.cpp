#include "MenuNav.hpp"

#define WINDOW_HEIGHT 600.f

MenuNav::MenuNav() : m_hasFocus(true)
{

}

MenuNav::~MenuNav()
{
	m_buttons.clear();
}

void MenuNav::addButton(const MenuButton button)
{
	m_buttons.push_back(button);
}

void MenuNav::giveFocusToShape(const unsigned short int& i)
{
	for(unsigned short int il=0;il<m_buttons.size();++il)
	{
		m_buttons[il].unHover();
		m_buttons[il].setFillColor(sf::Color(232, 131, 52, 100));
	}
	m_buttons[i].hover();
	m_buttons[i].setFillColor(sf::Color(255, 255, 255, 230));

	m_focusedShape = i;
}

void MenuNav::sort()
{
	m_buttons[0].setPosition(0.f,WINDOW_HEIGHT-m_buttons[0].getGlobalBounds().height);
	for(unsigned int i=1U; i< m_buttons.size();i++)
	{
		m_buttons[i].setPosition(0.f,m_buttons[i-1].getPosition().y-m_buttons[i].getGlobalBounds().height);
	}
}

void MenuNav::activate()
{
	m_buttons[m_focusedShape].hover();
	m_hasFocus = true;
}

void MenuNav::deactivate()
{
	m_buttons[m_focusedShape].unHover();
	m_buttons[m_focusedShape].setFillColor(sf::Color(255, 255, 255, 100));
	m_hasFocus = false;
}