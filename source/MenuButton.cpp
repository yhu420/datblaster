#include "MenuButton.hpp"

MenuButton::MenuButton(sf::Texture* NormalTexture, sf::Texture* hoveredTexture, sf::Texture* clicktex, const sf::Vector2f& position, const sf::Vector2f& dimensions) : RoundRect(position , dimensions ,5.f,3U, NormalTexture), isHovered(false), isUsed(false)
{
	m_normTex = NormalTexture;
	m_hoverTex = hoveredTexture;
	m_clickedTex = clicktex;
	m_attribute = nullptr;

	setOutlineColor(sf::Color(47, 255, 59));
	setOutlineThickness(0U);
	setFillColor(sf::Color(255, 255, 255, 200));
}

MenuButton::~MenuButton()
{
	delete m_attribute;
	m_attribute = nullptr;
}

void MenuButton::hover()
{
	isHovered = true;
	setTexture(m_hoverTex);
	rotate(randNumber(-30.f, 30.f));
	
	setFillColor(sf::Color(255, 255, 255, 255));
	setOutlineThickness(2U);
}

void MenuButton::unHover()
{
	isHovered = false;
	setTexture(m_normTex);
	rotate(-getRotation());

	setOutlineThickness(0U);
	setFillColor(sf::Color(255, 255, 255, 200));
}

void MenuButton::setActivated(const bool& activated)
{
	if(activated)
		setTexture(m_clickedTex);

	else
		setTexture(m_normTex);
}

void MenuButton::setAttribute(FullSprite* s)
{
	//TODO, and 2 years after the beginning of the game, to completely REWRITE, THIS IS PIECE OF VOMIT :$
}