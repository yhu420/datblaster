#pragma once
#include <SFML\Graphics.hpp>
#include "Settings.hpp"
#include "GlobalVars.h"

class GameHUD : public sf::Drawable
{
public:
	GameHUD(unsigned* gold, unsigned* score);
	~GameHUD();
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		target.draw(m_healthBarBackground);
		target.draw(m_healthBarForeground);

		target.draw(m_currentGold);
		target.draw(m_currentLevel);
		target.draw(m_currentScore);

		if (m_levelAnnounce)
			target.draw(*m_levelAnnounce);
	}

	void drawWaveText(const unsigned short& wavenumber);
	void updateLabels(); //this has to be called every time a ship 'dies'
	void removeWaveText();

private:
	sf::Texture			m_healthBackgroundTex;
	sf::Texture			m_healthForegroundTex;

	sf::Sprite			m_healthBarBackground;
	sf::Sprite			m_healthBarForeground;
	
	sf::Text			m_currentGold;
	sf::Text			m_currentLevel;
	sf::Text			m_currentScore;

	sf::Text*			m_levelAnnounce; //This is the "LEVEL 1" text which is displayed for the first seconds of the stage

	unsigned*			m_toScore;
	unsigned*			m_toMoney;
};