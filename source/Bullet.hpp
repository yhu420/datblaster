#pragma once 
#include "BulletModel.hpp"

class Bullet : public sf::Drawable
{
public:
	Bullet(
		sf::Vector2f const & position, 
		Pattern const & pattern, 
		BulletModel const & model);

	sf::Sprite const& getSprite() const {return m_sprite;};
	sf::Sprite& getNotConstSprite() {return m_sprite;};

	void followPattern();
	bool hasReachedEndOfPattern();
	int getDamage() const;

	//TODO arrange that so it matches the config files
	bool const isOutOfScreen() const{return	m_sprite.getPosition().x > 1030.f || 
												m_sprite.getPosition().x < -30.f || 
												m_sprite.getPosition().y > 630.f ||
												m_sprite.getPosition().y < -30.f;};
	
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const
	{
		target.draw(m_sprite);
	};

private:

	BulletModel		const&			m_model;
	sf::Sprite						m_sprite;
	Pattern							m_pattern;
};