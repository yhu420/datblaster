#include "ActualGame.hpp"

ActualGame::ActualGame()
	: m_gameHUD(&m_gold, &m_score),
	m_playerSpeedRight(Settings::getSingleValue("resources/others/gameplay.cfg", "playerSpeedRight").toFloat()),
	m_playerSpeedLeft(Settings::getSingleValue("resources/others/gameplay.cfg", "playerSpeedLeft").toFloat()),
	m_playerSpeedUp(Settings::getSingleValue("resources/others/gameplay.cfg", "playerSpeedUp").toFloat()),
	m_playerSpeedDown(Settings::getSingleValue("resources/others/gameplay.cfg", "playerSpeedDown").toFloat()),
	m_playerWalkableAreaMinX(Settings::getSingleValue("resources/others/gameplay.cfg", "walkableMinX").toFloat()),
	m_playerWalkableAreaMaxX(Settings::getSingleValue("resources/others/gameplay.cfg", "walkableMaxX").toFloat()),
	m_playerWalkableAreaMinY(Settings::getSingleValue("resources/others/gameplay.cfg", "walkableMinY").toFloat()),
	m_playerWalkableAreaMaxY(Settings::getSingleValue("resources/others/gameplay.cfg", "walkableMaxY").toFloat())
{	
	initModels();
	m_weaponHolder.push_back(Weapon(m_weaponModelHolder[0]));
	
	m_playerShip = make_unique<SpaceShip>(sf::Vector2f(
		Settings::getSingleValue("resources/others/gameplay.cfg", "playerSpawnX").toFloat(), 
		Settings::getSingleValue("resources/others/gameplay.cfg", "playerSpawnY").toFloat()), 
		m_weaponHolder[0], 
		m_shipPatternHolder[0], 
		m_SpaceShipModelHolder[0]);

	m_playerShip->setRotation(90);
	m_timeSinceLastSpawn.restart();
	m_gold = Var.gSettings.getValue("globalMoney").toInt();
	m_score = 0;

	m_canResumeSpawns = false;
	m_canEndCurrentWave = false;
	m_waveNumber = 1U;
	prepareNewWave();
}

ActualGame::~ActualGame()
{

}

void ActualGame::prepareNewWave()
{
	//Let's load values
	Settings tempMeta;
	tempMeta.setFileToParse("resources/others/levels.cfg");
	m_maxAllowedShipsOnScreenThisWave = tempMeta.getValue(to_string(m_waveNumber) + "_MaxOnScreen" ).toInt();
	m_minTimeForNewSpawn = tempMeta.getValue(to_string(m_waveNumber) + "_minSpawnTime").toFloat();

	//Let's save values
	//Values have to be saved on players death, but if game crashes before, or mom made dinner, at least its saved
	Var.gSettings.changeValue("globalMoney", to_string(m_gold));
	if (m_score > (unsigned)Var.gSettings.getValue("highscore").toInt())
	{
		Var.gSettings.changeValue("highscore", to_string(m_score));
	}
	Var.gSettings.save();

	//Let's fill the Spawn vector
	//For every model
	if (m_maxAllowedShipsOnScreenThisWave)
	{
		for (int i = 0; i < Var.gSettings.getValue("numberOfShipModels").toInt(); i++)
		{
			//For every ship
			for (int j = 0; j < tempMeta.getValue(to_string(m_waveNumber) + "_ShipModel" + to_string(i)).toInt(); j++)
			{
				m_remainingSpawn.push_back(i); //i is ship model, and j is the amount of them we want to spawn
			}
		}

		random_shuffle(m_remainingSpawn.begin(), m_remainingSpawn.end());
	}
	else
	{
		//Fill the spawning queue with random values
		//Step 1, find how many we want to spawn
		const int howManyMultiplier = tempMeta.getValue("allCompleteMultiplier").toInt();
		const int howManyModels = Var.gSettings.getValue("numberOfShipModels").toInt();
		std::random_device rd;

		for (int i = 0; i < m_waveNumber * howManyMultiplier; i++)
		{
			//Step 2 fill the vector with random ids
			m_remainingSpawn.push_back(rd() % howManyModels);
		}

		m_maxAllowedShipsOnScreenThisWave = 50;
		m_minTimeForNewSpawn = tempMeta.getValue("allCompleteSpawnTime").toFloat();
	}

	//Let's debug

	std::cout << "\n=======Beginning Wave " << m_waveNumber << "=======" << endl;
	std::cout << "Max Allowed on Screen : " << m_maxAllowedShipsOnScreenThisWave << endl;
	std::cout << "Spawning " << m_remainingSpawn.size() << " ships this wave." << endl;

	//Draw text, maybe leave an upgrade from time to time
	m_gameHUD.drawWaveText(m_waveNumber);

	//Finally
	m_waveNumber++;
	m_cooldownBetweenWaves.restart();
	m_canResumeSpawns = false;
}

void ActualGame::spawnNewShip(sf::Vector2f const& position, SpaceShipModel const& model)
{
	m_spaceShipHolder.push_back( make_unique<SpaceShip>(
		position, 
		m_weaponHolder[model.getWeaponID()], 
		m_shipPatternHolder[model.getPatternID()],
		model)
	);
	m_timeSinceLastSpawn.restart();
}

void ActualGame::spawnNewBullet(SpaceShip& shooter)
{
	shooter.fakeFire();
	m_bulletHolder.push_back(make_unique<Bullet>(
		shooter.getSprite().getPosition() + shooter.getStartPoint(),
		m_bulletPatternHolder[shooter.getWeapon().getBulletPatternID()].shufflePattern(shooter.getWeapon().getImprecision()),
		m_bulletModelHolder[shooter.getWeapon().getBulletModelID()]
		));
}

void ActualGame::spawnNewDyingCharacter(sf::Vector2f const & position, std::string const & pathNoExt, sf::Texture const& text)
{
	m_dyingCharacters.push_back(std::make_unique<AnimatedSprite>(pathNoExt));
	m_dyingCharacters.back()->setTexture(text);	
	m_dyingCharacters.back()->setPosition(position);
}

bool ActualGame::autoShipSpawn()
{
	if (m_remainingSpawn.empty() && m_spaceShipHolder.empty())
	{
		//end the wave
		return true;
	}
	else if (!m_remainingSpawn.empty() && m_maxAllowedShipsOnScreenThisWave > m_spaceShipHolder.size() && m_timeSinceLastSpawn.getElapsedTime().asSeconds() > m_minTimeForNewSpawn)
	{
		//Spawn the next new ship from the list
		//The list should have been mixed beforethe beginning of the wave TODO

		//First , generate a random position at the right of the screen

		//TODO : replace 1010 with desired Var.gsettings.windowedres + user defined 'out of screen' range
		//Todo: replace the y position too
		spawnNewShip(sf::Vector2f(Var.gSettings.getValue("windowedWidth").toFloat() + Var.gSettings.getValue("outOfScreenMaxDist").toFloat() - 1,
			randNumber<float>(0, Var.gSettings.getValue("windowedHeight").toFloat())),
			m_SpaceShipModelHolder[m_remainingSpawn.back()]);
		m_remainingSpawn.pop_back();
	}
	return false;
}

void ActualGame::run()
{

	/* ****************************************************************************** */
	/* *******************************PLAYER Controls******************************** */
	/* ****************************************************************************** */
	if (m_playerShip->getHealth() > 0)
	{
		/* ****************************************************************************** */
		/* *******************************MOVEMENT OF PLAYER***************************** */
		/* ****************************************************************************** */

		if ( sf::Keyboard::isKeyPressed(Var.goLeft) && m_playerShip->getSprite().getPosition().x > m_playerWalkableAreaMinX)
			m_playerShip->move(-m_playerSpeedLeft * Var.deltaTime, 0.f);

		if ( sf::Keyboard::isKeyPressed(Var.goRight) && m_playerShip->getSprite().getPosition().x < m_playerWalkableAreaMaxX)
			m_playerShip->move(m_playerSpeedRight * Var.deltaTime, 0.f);

		if ( sf::Keyboard::isKeyPressed(Var.goUp) && m_playerShip->getSprite().getPosition().y > m_playerWalkableAreaMinY)
			m_playerShip->move(0.f, -m_playerSpeedUp * Var.deltaTime);

		if ( sf::Keyboard::isKeyPressed(Var.goDown) && m_playerShip->getSprite().getPosition().y < m_playerWalkableAreaMaxY)
			m_playerShip->move(0.f, m_playerSpeedDown * Var.deltaTime);

		/* ****************************************************************************** */
		/* ********************************ACTIONS OF PLAYER***************************** */
		/* ****************************************************************************** */

		if (sf::Keyboard::isKeyPressed(Var.fire) && m_playerShip->canFire())
		{
			spawnNewBullet(*m_playerShip);
		}
	
	}//=> m_playtxter controls

	 /* ****************************************************************************** */
	 /* **************************CALCULTAING BULLET COLLISION************************ */
	 /* ****************************************************************************** */

	//Reversing backwards because of the erase method, which will 
	//Shift every index back, I don't know if it's really necessary,
	//But it works just fine
	for (std::size_t i = m_bulletHolder.size(); i-- > 0;)
	{
		for (std::size_t j = m_spaceShipHolder.size(); j-- > 0;)
		{
			//Two level collision. First check with the bounds
			if (m_bulletHolder[i]->getSprite().getGlobalBounds().intersects(m_spaceShipHolder[j]->getSprite().getGlobalBounds()))
			{
				//TODO
				//Check collision with hitboxes

				//ALPHA
				//This function really sucks, this will be #1 on todo list
				//It seems to return true all the time

				if (shapesCollideCustom(m_bulletHolder[i]->getSprite().getGlobalBounds(), m_spaceShipHolder[j]->getHitbox())) //That's where we are supposed to to the hitbox check with shapes
				{
					m_spaceShipHolder[j]->receiveDamage(m_bulletHolder[i]->getDamage());
					m_bulletHolder.erase(m_bulletHolder.begin() + i);
					//The bullet has been removed, we now have to prevent other checks with the bullet.
					//that's why we set j = 0 at the end.
					

					if (m_spaceShipHolder[j]->getHealth() < 0)
					{
						m_score += m_spaceShipHolder[j]->getScoreReward();
						m_gold += m_spaceShipHolder[j]->getMoneyReward();
						m_gameHUD.updateLabels();

						spawnNewDyingCharacter(m_spaceShipHolder[j]->getSprite().getPosition(), m_spaceShipHolder[j]->getModel().getDyingSpritePath(), m_spaceShipHolder[j]->getModel().getDeathTexture());
						m_spaceShipHolder.erase(m_spaceShipHolder.begin() + j);
					}
					
					j = 0; //Bullet is deleted on hit. No need to check with other entities.
					//If we want piercing bullets, we will have to rewrite that part.
				}
			}
		}
	}


	/* ****************************************************************************** */
	/* **************************DELETIN / MOVIN BULLETS***************************** */
	/* ****************************************************************************** */

	for ( std::size_t i = 0; i < m_bulletHolder.size(); i++ )
	{
		m_bulletHolder[i]->followPattern();
		if ( m_bulletHolder[i]->isOutOfScreen() || m_bulletHolder[i]->hasReachedEndOfPattern())
			m_bulletHolder.erase(m_bulletHolder.begin() + i); //Does the unique ptr delete the bullet? gotta double check that
	}
	/* ****************************************************************************** */
	/* ******************************ANIMATING SHIPS********************************* */
	/* ****************************************************************************** */

	for (size_t i = 0; i < m_spaceShipHolder.size(); i++)
	{
		m_spaceShipHolder[i]->getSprite().playAnimationLooped();
	}
	for (size_t i = 0; i < m_dyingCharacters.size(); i++)
	{
		if (!m_dyingCharacters[i]->playAnimation())
			m_dyingCharacters.erase(m_dyingCharacters.begin() + i);
	}
	m_playerShip->getSprite().playAnimationLooped();

	/* ****************************************************************************** */
	/* *******************************SPAWNING SHIPS********************************* */
	/* ****************************************************************************** */
	if (m_canResumeSpawns)
	{
		if (autoShipSpawn())
		{
			prepareNewWave();
		}
	}
	else
	{
		if (m_cooldownBetweenWaves.getElapsedTime().asSeconds() > 4)
			m_gameHUD.removeWaveText();
		m_canResumeSpawns = m_cooldownBetweenWaves.getElapsedTime().asSeconds() > 5;
	}


	/* ****************************************************************************** */
	/* ********************************DELETIN SHIPS********************************* */
	/* ****************************************************************************** */
	//If outta screen
	for (std::size_t i = 0; i < m_spaceShipHolder.size(); i++)
	{
		if (m_spaceShipHolder[i]->getSprite().getPosition().x < -Var.gSettings.getValue("outOfScreenMaxDist").toFloat() || 
			m_spaceShipHolder[i]->getSprite().getPosition().y < -Var.gSettings.getValue("outOfScreenMaxDist").toFloat() ||
			m_spaceShipHolder[i]->getSprite().getPosition().y > Var.gSettings.getValue("windowedHeight").toFloat() + Var.gSettings.getValue("outOfScreenMaxDist").toFloat() ||
			m_spaceShipHolder[i]->getPattern().reachedEnd())
		{
			m_spaceShipHolder.erase(m_spaceShipHolder.begin() + i);
		}
	}

	//If death --> TODO

	/* ****************************************************************************** */
	/* *********************************MOVING SHIPS********************************* */
	/* ****************************************************************************** */
	for (std::size_t i = 0; i < m_spaceShipHolder.size(); i++)
	{
		m_spaceShipHolder[i]->followPattern();
	}

	/* ****************************************************************************** */
	/* ********************************HUD UPDATE************************************ */
	/* ****************************************************************************** */

}

void ActualGame::initModels()
{
	Settings sett;
	sf::ConvexShape hitboxes;

	/* ****************************************************************************** */
	/* *******************************SHIP MODELS************************************ */
	/* ****************************************************************************** */
	for (int i = 0; i < Var.gSettings.getValue("numberOfShipModels").toInt(); i++)
	{
		sett.clearValues();
		sett.setFileToParse("resources/others/shipModel" + std::to_string(i) + ".cfg");
		hitboxes = sf::ConvexShape(sett.getValue("points").toInt());

		//Second loop for every point of the hitbox
		for (int j = 0; j < sett.getValue("points").toInt(); j++)
		{
			hitboxes.setPoint(j, sf::Vector2f(float(sett.getValue("ptx" + std::to_string(j)).toInt()), float(sett.getValue("pty" + std::to_string(j)).toInt())));
		}

		m_SpaceShipModelHolder.push_back(
			SpaceShipModel(
				"resources/images/game/ships/ship" + sett.getValue("spriteID").toString(),	//Texture path
				hitboxes,															//hitbox
				sf::Vector2f(														//Start point
					sett.getValue("startPointx").toFloat(),
					sett.getValue("startPointy").toFloat()),
					sett.getValue("defaultHealth").toInt(),								//health default
					sett.getValue("defaultSpeed").toFloat(),							//Speed default
					sett.getValue("patternID").toInt(),									//Pattern ID
					sett.getValue("weaponID").toInt(),									//Weapon ID
					sett.getValue("score").toInt(),										//Score Reward
					sett.getValue("money").toInt(),										//Money Reward
					sett.getValue("patternImprecision").toFloat(),
					sett.getValue("patternRelative").toBool(),
					"resources/images/game/ships/shipDeath" + sett.getValue("onDeathSpriteID").toString(),	//Texture death path
					"resources/images/game/ships/shipHit" + sett.getValue("onHitSpriteID").toString()	//Texture hit path
		));
	}

	/* ****************************************************************************** */
	/* *******************************WEAPON MODELS********************************** */
	/* ****************************************************************************** */

	for (int i = 0; i < Var.gSettings.getValue("numberOfWeaponModels").toInt(); i++)
	{
		sett.clearValues();
		sett.setFileToParse("resources/others/wpnModel" + std::to_string(i) + ".cfg");
		
		m_weaponModelHolder.push_back(WeaponModel(
			"resources/sound/effects/gunSound" + std::to_string(i) + ".wav",
			sett.getValue("fireRate").toInt(),
			sett.getValue("defaultBulletPattern").toInt(),
			sett.getValue("defaultBulletModel").toInt(),
			sett.getValue("imprecision").toFloat()
			));
	}
	
	/* ****************************************************************************** */
	/* *******************************BULLET MODELS********************************** */
	/* ****************************************************************************** */

	//BULLET PATTERNS
	std::vector<sf::Vector2f> temp;
	std::vector<float> tempbis;
	for (int i = 0; i < Var.gSettings.getValue("numberOfBulletPatterns").toInt(); i++)
	{
		sett.clearValues();
		sett.setFileToParse("resources/others/bulletpattern" + std::to_string(i) + ".cfg");
		for (int j = 0; j < sett.getValue("total").toInt(); j++)
		{
			temp.push_back(sf::Vector2f(sett.getValue("x" + to_string(j)).toFloat(), sett.getValue("y" + to_string(j)).toFloat()));
		}
		for (int j = 0; j < sett.getValue("total").toInt() - 2; j++)
		{
			tempbis.push_back(sett.getValue("m" + to_string(j)).toFloat());
		}
		
		m_bulletPatternHolder.push_back(Pattern(temp, tempbis, 0.f));
		temp.clear();
		tempbis.clear();
	}

	//BULLET MODELS
	for (int i = 0; i < Var.gSettings.getValue("numberOfBulletModels").toInt(); i++)
	{
		sett.clearValues();
		sett.setFileToParse("resources/others/bulletModel" + std::to_string(i) + ".cfg");
		m_bulletModelHolder.push_back(BulletModel(
			"resources/images/game/ammo/ammoType" + sett.getValue("ammoImg").toString() + ".png",
			sett.getValue("speed").toFloat(),
			sett.getValue("damage").toInt()
			)
		);
	}

	/* ****************************************************************************** */
	/* *******************************PATTERN OF SHIP******************************** */
	/* ****************************************************************************** */
	
	temp.clear();
	tempbis.clear();
	for (int i = 0; i < Var.gSettings.getValue("numberOfEnemyPatterns").toInt(); i++)
	{
		sett.clearValues();
		sett.setFileToParse("resources/others/enemypattern" + std::to_string(i) + ".cfg");
		for (int j = 0; j < sett.getValue("total").toInt(); j++)
		{
			temp.push_back(sf::Vector2f(sett.getValue("x" + to_string(j)).toFloat(), sett.getValue("y" + to_string(j)).toFloat()));
		}
		
		for (int j = 0; j < sett.getValue("total").toInt() - 1; j++)
		{
			tempbis.push_back(sett.getValue("m" + to_string(j)).toFloat());
		}

		m_shipPatternHolder.push_back(Pattern(temp, tempbis, 0.f));
		temp.clear();
		tempbis.clear();
	}
	
}