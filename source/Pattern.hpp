#pragma once
#include <SFML/Graphics.hpp>
#include "Basic_funcs.hpp"

class Pattern
{
public:
	Pattern(std::vector<sf::Vector2f> const& valueTable, std::vector<float> const& speedy, const float& imprecision = 0.f);
	Pattern shufflePattern(float const& radiusToShuffle) const;
	void shuffleSelfPattern(float const& radiusToShuffle);
	sf::Vector2f getMovingVector(sf::Vector2f const& currentPosition);
	
	void setNodePos(std::size_t const& index, sf::Vector2f const& newpos);
	bool reachedEnd() const;

	//Complicated name for ShiftPattern, sort of
	void moveAllNodesSoFirstOneIsLike(sf::Vector2f const& firstOne);

private:
	std::vector<sf::Vector2f>	m_nodes;
	std::vector<float>			m_speedModif;
	std::vector<bool>			m_rotateToLookForwardModif; //I cant exactly remember hy I did this, delete this?
	std::size_t					m_lastNodeIndex;
};