#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>

using namespace std;

class SettingsString
{
public :
	SettingsString(string value)
	{
		stringvalue = value;
	}
	SettingsString() {}
	int toInt() const
	{
		if(stringvalue.size() == 0)
		{
			cerr << "Hmm the string you want to return ( to int ) is empty.. just lettin' you know." << endl;
		}
		return atoi(stringvalue.c_str());
	}
	double toDouble() const
	{
		if(stringvalue.size() == 0)
		{
			cerr << "Hmm the string you want to return ( to double ) is empty.. just lettin' you know." << endl;
		}
		return atof(stringvalue.c_str());
	}
	float toFloat() const
	{
		if(stringvalue.size() == 0)
		{
			cerr << "Hmm the string you want to return ( to float ) is empty.. just lettin' you know." << endl;
		}
		return float(atof(stringvalue.c_str()));
	}
	string toString() const
	{
		if(stringvalue.size() == 0)
		{
			cerr << "Hmm the string you want to return ( to string ) is empty.. just lettin' you know." << endl;
		}
		return stringvalue;
	}
	char toChar() const
	{
		if(stringvalue.size() != 1)
		{
			cerr << "Could not convert as a char, the value is different than one letter long.\n'Z' will automatically be returned." << endl;
			return 'Z';
		}
		else
		{
			return stringvalue[0];
		}
	}
	long int toLongInt() const
	{
		if(stringvalue.size() == 0)
		{
			cerr << "Hmm the string you want to return ( to long int ) is empty.. just lettin' you know." << endl;
		}
		return atol(stringvalue.c_str());
	}
	bool toBool() const
	{
		if(stringvalue.size() == 0)
		{
			cerr << "Hmm the string you want to return ( to long int ) is empty.. just lettin' you know." << endl;
		}
		return stringvalue != "0";
	}

private:
	string stringvalue;
};

class Settings
{
public:
	Settings();
	Settings(string filename);
	bool setFileToParse(const string filename);

	static SettingsString getSingleValue(const std::string& file, const std::string& key);

	//This methode only takes strings, if you want to write ints  floats or else, just google how to convert T to string
	void addKey(string key, string value);

	//Returns true on success and false on failure
	bool removeKey(const string key);

	SettingsString getValue(const string key) const;

	string getFilePath() const { return filepath;}

	void printAllValues() const;

	bool changeValue(const string key, const string newValue);

	void save() const;

	void clearValues();
private:
	//true if everything went right
	bool create();
	vector<string> list;
	ifstream in_stream;
	string line;
	string filepath;
	map<string, SettingsString> mapOfDoom;
};